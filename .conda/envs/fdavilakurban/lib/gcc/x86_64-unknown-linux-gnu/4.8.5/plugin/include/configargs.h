/* Generated automatically. */
static const char configuration_arguments[] = "./configure --prefix=/home/fdavilakurban/.conda/envs/fdavilakurban --with-gxx-include-dir=/home/fdavilakurban/.conda/envs/fdavilakurban/gcc/include/c++ --bindir=/home/fdavilakurban/.conda/envs/fdavilakurban/bin --datarootdir=/home/fdavilakurban/.conda/envs/fdavilakurban/share --libdir=/home/fdavilakurban/.conda/envs/fdavilakurban/lib --with-gmp=/home/fdavilakurban/.conda/envs/fdavilakurban --with-mpfr=/home/fdavilakurban/.conda/envs/fdavilakurban --with-mpc=/home/fdavilakurban/.conda/envs/fdavilakurban --with-isl=/home/fdavilakurban/.conda/envs/fdavilakurban --with-cloog=/home/fdavilakurban/.conda/envs/fdavilakurban --enable-checking=release --with-tune=generic --disable-multilib";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" }, { "tune", "generic" } };
