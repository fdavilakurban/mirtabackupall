#
# Configuration file for using the xslt library
#
XSLT_LIBDIR="-L/home/fdavilakurban/.conda/envs/fdavilakurban/lib"
XSLT_LIBS="-lxslt  -L/home/fdavilakurban/.conda/envs/fdavilakurban/lib -lxml2 -lz -lm -ldl -lm -lrt"
XSLT_INCLUDEDIR="-I/home/fdavilakurban/.conda/envs/fdavilakurban/include"
MODULE_VERSION="xslt-1.1.28"
