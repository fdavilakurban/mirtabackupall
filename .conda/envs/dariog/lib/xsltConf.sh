#
# Configuration file for using the xslt library
#
XSLT_LIBDIR="-L/home/fdavilakurban/.conda/envs/dariog/lib"
XSLT_LIBS="-lxslt  -L/home/fdavilakurban/.conda/envs/dariog/lib -lxml2 -lz -lm -ldl -lm -lrt"
XSLT_INCLUDEDIR="-I/home/fdavilakurban/.conda/envs/dariog/include"
MODULE_VERSION="xslt-1.1.28"
