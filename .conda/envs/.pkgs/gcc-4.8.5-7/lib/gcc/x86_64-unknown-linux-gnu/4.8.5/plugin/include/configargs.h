/* Generated automatically. */
static const char configuration_arguments[] = "./configure --prefix=/opt/anaconda1anaconda2anaconda3 --with-gxx-include-dir=/opt/anaconda1anaconda2anaconda3/gcc/include/c++ --bindir=/opt/anaconda1anaconda2anaconda3/bin --datarootdir=/opt/anaconda1anaconda2anaconda3/share --libdir=/opt/anaconda1anaconda2anaconda3/lib --with-gmp=/opt/anaconda1anaconda2anaconda3 --with-mpfr=/opt/anaconda1anaconda2anaconda3 --with-mpc=/opt/anaconda1anaconda2anaconda3 --with-isl=/opt/anaconda1anaconda2anaconda3 --with-cloog=/opt/anaconda1anaconda2anaconda3 --enable-checking=release --with-tune=generic --disable-multilib";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" }, { "tune", "generic" } };
