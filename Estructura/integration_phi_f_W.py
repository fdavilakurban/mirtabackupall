Hoy # -*- coding: utf-8 -*-
"""

"""
#import numpy as np
#import scipy.integrate as integrate

#%%
phiStar = 5E-3
alpha = -1.
MStar = -20.5

def schechter(magnitude, phiStar=phiStar, alpha=alpha, MStar=MStar): 
    """Schechter luminosity function by magnitudes.""" 
    MStarMinM = 0.4 * (MStar - magnitude) 
    return (0.4 * np.log(10.) * phiStar * 10.0**(MStarMinM * (alpha + 1.)) *
            np.exp(-10.**MStarMinM)) 

Mmax = -24.
Mmin = -12.

mrange = np.linspace(Mmax,Mmin,10000)

plt.plot(mrange,np.log10(schechter(mrange)))

#%%
I_n = integrate.quad(schechter,Mmax,Mmin)
n = I_n[0]
print n

#%%
#def step(x):
#    return 1 * ((x > -18.)&(x < -17.))

#plt.step(mrange,step(mrange))

#%%
#plt.plot(mrange,schechter(mrange)*step(mrange))

#%%
#def integrand(x):
#    return schechter(x)*step(x)/n

#I_phi = integrate.quad(integrand,Mmax,Mmin)
#phi = I_phi[0]
#print phi

#%% 
#"""Ejemplo"""
from sympy import integrate as sintegrate
from sympy import Symbol, exp
from sympy.abc import x

#y = Symbol('y')
#f = x*exp(x/y)
#sintegrate(f, (x, 1, 2))

#%%

def step(x):
    y = Symbol('y')
    return 1 * (((x-MStar+5.*np.log10(y))<Mmin)&((x-MStar+5.*np.log10(y))>Mmax))

def integrand(x):
    return schechter(x)*step(x)/n

sintegrate(integrand(mrange), (x,-24.,-16.))

#%%
"""
Integral phi(y) = int(Phi(M)*f(M-M*+5logy)/n)
"""
#m1 = 17.
m2 = 21.
D = 10.**((m2-MStar-25.)/5.) #pc
#D /= 1E6 #Mpc
y = np.linspace(0.,4./D,1000)
y *= D
phi = []

for i in y:
    Mlim1 = MStar - 5.*np.log10(i)
    Mlim2 = 2.+ MStar - 5.*np.log10(i)
    I_phi = integrate.quad(schechter,Mlim1,Mlim2) 
    phi_n = I_phi[0] / n
    phi.append(phi_n)

plt.plot(y,phi)
plt.plot(y,y**2.*phi)
#%% Lo mismo que la celda anterior, pero con otra forma para f(m-m0)

m1 = 17.
m2 = 21.

D = 10.**((m1-MStar-25.)/5.) #Mpc
y = np.linspace(0.,100./D,1000)
y *= D
phi = []

M1 = m1 - 5.*np.log10(y) - 25.
M2 = m2 - 5.*np.log10(y) - 25.


Mlim2 = -12.
Mlim1 = -26.
        

def f(x,mx1,mx2):
    return np.where((x>mx1)&(x<mx2),1.,0.)

#f = np.where()

for i in range(len(y)):
    yx = y[i]
    mx1 = M1[i]
    mx2 = M2[i]
    def integrand(x):
        #global mx1
        #global mx2
        return schechter(x)*f(x,mx1,mx2)

    I_phi = integrate.quad(integrand,Mlim1,Mlim2) 
    phi_n = I_phi[0] / n
    phi.append(phi_n)

plt.plot(y,phi)
plt.plot(y,y**2.*phi)


#%%

gamma = -1.7
def xi(u, x, y, A=1., gamma = gamma):
    return A*(u**2.+(x*y)**2.)**(gamma/2.)
#%%

def I_xi(x, y, A=1., gamma = gamma):
    return integrate.quad(lambda u: A*(u**2.+(x*y)**2.)**(gamma/2.), -np.inf, np.inf)

def I_Yphi():
    return 

    

