# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi


#module load anaconda-2.1.0

module load apps/anaconda2
module load compilers/gcc



# User specific aliases and functions

alias g++="/opt/compilers/gcc/5.3.0/bin/g++ --std=c++0x"
alias gcc="/opt/compilers/gcc/5.3.0/bin/gcc"

source activate fdavilakurban


clear
