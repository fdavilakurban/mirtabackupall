# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np

def schechter(magnitude, phiStar=5.96E-11, alpha=-1.35, MStar=-20.5): 
    """Schechter luminosity function by magnitudes.""" 
    MStarMinM = 0.4 * (MStar - magnitude) 
    return (0.4 * np.log(10) * phiStar * 10.0**(MStarMinM * (alpha + 1.)) *
            np.exp(-10.**MStarMinM)) 
    
schechter_range = np.logspace(8.,12.,10000)

plt.scatter(schechter_range,schechter(schechter_range+0j))
