plt.hist(gals1['col1'], bins=50, histtype='step',linewidth=1.8,linestyle='solid', normed=True, color='k', alpha=1,label='Pairs')#, rwidth=rwidth)
plt.hist(gals2['col1'], bins=47, histtype='step',linewidth=1.8,linestyle='solid', normed=True, color='blue', alpha=1,label='Triplets')#, rwidth=rwidth)
plt.hist(gals3['col1'], bins=25, histtype='step',linewidth=1.8,linestyle='solid', normed=True, color='red', alpha=1,label='Minor Groups')#, rwidth=rwidth)
#plt.hist(gals4['col1'], bins=16, histtype='step',linewidth=1.8,linestyle='solid', normed=False, color='g', alpha=1,label=sample4)#, rwidth=rwidth)

plt.xlim(-5.,20.)
#plt.gca().set_xscale("log")

if stat == 'fixap':
	plt.title('${}Mpc\leq r_p\leq {}Mpc\,;\, \Delta V\leq {}Km/s$'.format(rpmin,rpmax,dV),fontsize=18)

if stat == 'sigma':
	plt.xlabel('$\log (\Sigma_{5})$',fontsize=18)

if stat == 'fixap':
	plt.xlabel('$(N-Nran)/Nran$',fontsize=18)

#plt.xlabel('$n/V$',fontsize=18)
plt.ylabel('Normalized Distribution',fontsize=13)
plt.tight_layout()
plt.legend(fancybox=True,frameon=True,shadow=True,loc=1)
plt.show()    
if stat == 'sigma':
	plt.savefig('Plots/sigma5.pdf')

if stat == 'fixap':
	#cd Plots
	plt.savefig('Plots/fixap_{}-{}-{}.pdf'.format(rpmin,rpmax,dV))


plt.close()
bins=10
rwidth=.25
plt.hist(gals1['col1'], bins=50, histtype='step',linewidth=1.8,linestyle='solid', normed=True, color='k', alpha=1,label='Pairs')#, rwidth=rwidth)
plt.hist(gals2['col1'], bins=47, histtype='step',linewidth=1.8,linestyle='solid', normed=True, color='blue', alpha=1,label='Triplets')#, rwidth=rwidth)
plt.hist(gals3['col1'], bins=26, histtype='step',linewidth=1.8,linestyle='solid', normed=True, color='red', alpha=1,label='Minor Groups')#, rwidth=rwidth)
#plt.hist(gals4['col1'], bins=16, histtype='step',linewidth=1.8,linestyle='solid', normed=False, color='g', alpha=1,label=sample4)#, rwidth=rwidth)

plt.xlim(-5.,20.)
#plt.gca().set_xscale("log")

if stat == 'fixap':
	plt.title('${}Mpc\leq r_p\leq {}Mpc\,;\, \Delta V\leq {}Km/s$'.format(rpmin,rpmax,dV),fontsize=18)

if stat == 'sigma':
	plt.xlabel('$\log (\Sigma_{5})$',fontsize=18)

if stat == 'fixap':
	plt.xlabel('$(N-Nran)/Nran$',fontsize=18)

#plt.xlabel('$n/V$',fontsize=18)
plt.ylabel('Normalized Distribution',fontsize=13)
plt.tight_layout()
plt.legend(fancybox=True,frameon=True,shadow=True,loc=1)
plt.show()    
if stat == 'sigma':
	plt.savefig('Plots/sigma5.pdf')

if stat == 'fixap':
	#cd Plots
	plt.savefig('Plots/fixap_{}-{}-{}.pdf'.format(rpmin,rpmax,dV))



##---(Tue Aug 22 14:44:22 2017)---
runfile('/home/fdavilakurban/GESGG/histogramas.py', wdir='/home/fdavilakurban/GESGG')
runfile('/home/fdavilakurban/GESGG/fixap_final_1.py', wdir='/home/fdavilakurban/GESGG')
run imports
runfile('/home/fdavilakurban/GESGG/fixap_final_1.py', wdir='/home/fdavilakurban/GESGG')
rho_est
print 'File "{}" created'.format(filename)
rhoEstPares12 = ascii.read('fixap_pares_1.0-2.0-1000_est.txt')
rhoEstPares12
rhoEstPares12 = ascii.read('fixap_pares_1.0-2.0-1000_est.txt',format='no_header')
rhoEstPares12
np.mean(rhoEstPares12)
np.mean(rhoEstPares12['col1'])
import bootstrap as bs
bs([1.,2.,1.],100)
import bootstrap.bootstrap as bs
runfile('/home/fdavilakurban/rhoest_means.py', wdir='/home/fdavilakurban')
ls
cd GESGG
ls
runfile('/home/fdavilakurban/GESGG/rhoest_means.py', wdir='/home/fdavilakurban/GESGG')
meanPares12
print 'Calculating Bootstrap errors' 

def bootstrap(data, num_samples):
    n = len(data)
    idx = npr.randint(0, n, (num_samples, n))
    samples = data[idx]
    stat = (np.median(samples, 1))
    return np.std(stat)


meanPares12_err = bootstrap(rhoEstPares12,100)
meanPares23_err = bootstrap(rhoEstPares23,100)
meanPares34_err = bootstrap(rhoEstPares34,100)
meanPares45_err = bootstrap(rhoEstPares45,100)

meanTripletes12_err = bootstrap(rhoEstTripletes12,100)
meanTripletes23_err = bootstrap(rhoEstTripletes23,100)
meanTripletes34_err = bootstrap(rhoEstTripletes34,100)
meanTripletes45_err = bootstrap(rhoEstTripletes45,100)

meanGmenores12_err = bootstrap(rhoEstGmenores12,100)
meanGmenores23_err = bootstrap(rhoEstGmenores23,100)
meanGmenores34_err = bootstrap(rhoEstGmenores34,100)
meanGmenores45_err = bootstrap(rhoEstGmenores45,100)

print 'Calculating Bootstrap errors' 

def bootstrap(data, num_samples):
    n = len(data)
    idx = np.random.randint(0, n, (num_samples, n))
    samples = data[idx]
    stat = (np.median(samples, 1))
    return np.std(stat)


meanPares12_err = bootstrap(rhoEstPares12,100)
meanPares23_err = bootstrap(rhoEstPares23,100)
meanPares34_err = bootstrap(rhoEstPares34,100)
meanPares45_err = bootstrap(rhoEstPares45,100)

meanTripletes12_err = bootstrap(rhoEstTripletes12,100)
meanTripletes23_err = bootstrap(rhoEstTripletes23,100)
meanTripletes34_err = bootstrap(rhoEstTripletes34,100)
meanTripletes45_err = bootstrap(rhoEstTripletes45,100)

meanGmenores12_err = bootstrap(rhoEstGmenores12,100)
meanGmenores23_err = bootstrap(rhoEstGmenores23,100)
meanGmenores34_err = bootstrap(rhoEstGmenores34,100)
meanGmenores45_err = bootstrap(rhoEstGmenores45,100)

idx = np.random.randint(0, n, (num_samples, n))
n = len(data)
meanPares12_err = bootstrap(rhoEstPares12,100)
print 'Calculating Bootstrap errors' 

def bootstrap(data, num_samples):
    n = len(data)
    idx = np.random.randint(0, n, (num_samples, n))
    samples = data[idx]
    stat = (np.median(samples, 1))
    return np.std(stat)


meanPares12_err = bootstrap(rhoEstPares12['col1'],100)
meanPares23_err = bootstrap(rhoEstPares23['col1'],100)
meanPares34_err = bootstrap(rhoEstPares34['col1'],100)
meanPares45_err = bootstrap(rhoEstPares45['col1'],100)

meanTripletes12_err = bootstrap(rhoEstTripletes12['col1'],100)
meanTripletes23_err = bootstrap(rhoEstTripletes23['col1'],100)
meanTripletes34_err = bootstrap(rhoEstTripletes34['col1'],100)
meanTripletes45_err = bootstrap(rhoEstTripletes45['col1'],100)

meanGmenores12_err = bootstrap(rhoEstGmenores12['col1'],100)
meanGmenores23_err = bootstrap(rhoEstGmenores23['col1'],100)
meanGmenores34_err = bootstrap(rhoEstGmenores34['col1'],100)
meanGmenores45_err = bootstrap(rhoEstGmenores45['col1'],100)

meanPares12_err
plt.boxplot?
plt.boxplot(rhoEstPares12)
plt.boxplot(rhoEstPares12['col1'])
plt.boxplot(rhoEstPares12['col1'],notch='True')
plt.boxplot(rhoEstPares12['col1'],notch='True',bootstrap=1000)
plt.boxplot(rhoEstPares12['col1'],notch='True',bootstrap=10)
plt.boxplot(rhoEstPares12['col1'],notch='True',bootstrap=10000)
plt.boxplot([rhoEstPares12['col1'],rhoEstTripletes['col1']],notch='True',bootstrap=1000)
plt.boxplot([rhoEstPares12['col1'],rhoEstTripletes12['col1']],notch='True',bootstrap=1000)
meanPares12,meanTripletes12
plt.boxplot([rhoEstPares12['col1'],rhoEstTripletes12['col1']],notch='True',bootstrap=1000)
plt.boxplot([rhoEstPares12['col1'],rhoEstTripletes12['col1'],rhoEstGmenores12['col1']],notch='True',bootstrap=1000)
plt.show()

plt.boxplot([rhoEstPares23['col1'],rhoEstTripletes23['col1'],rhoEstGmenores23['col1']],notch='True',bootstrap=1000)
plt.show()

plt.boxplot([rhoEstPares34['col1'],rhoEstTripletes34['col1'],rhoEstGmenores34['col1']],notch='True',bootstrap=1000)
plt.show()

plt.boxplot([rhoEstPares45['col1'],rhoEstTripletes45['col1'],rhoEstGmenores45['col1']],notch='True',bootstrap=1000)
plt.show()


plt.boxplot([rhoEstPares12['col1'],rhoEstTripletes12['col1'],rhoEstGmenores12['col1']],notch='True',bootstrap=1000)
plt.show()

plt.boxplot([rhoEstPares23['col1'],rhoEstTripletes23['col1'],rhoEstGmenores23['col1']],notch='True',bootstrap=1000)
plt.show()

plt.boxplot([rhoEstPares34['col1'],rhoEstTripletes34['col1'],rhoEstGmenores34['col1']],notch='True',bootstrap=1000)
plt.show()

plt.boxplot([rhoEstPares45['col1'],rhoEstTripletes45['col1'],rhoEstGmenores45['col1']],notch='True',bootstrap=1000)
plt.show()


plt.boxplot([rhoEstPares23['col1'],rhoEstTripletes23['col1'],rhoEstGmenores23['col1']],notch='True',bootstrap=1000)
plt.show()
plt.close()

plt.boxplot([rhoEstPares34['col1'],rhoEstTripletes34['col1'],rhoEstGmenores34['col1']],notch='True',bootstrap=1000)
plt.show()

plt.boxplot([rhoEstPares45['col1'],rhoEstTripletes45['col1'],rhoEstGmenores45['col1']],notch='True',bootstrap=1000)
plt.show()


plt.boxplot([rhoEstPares23['col1'],rhoEstTripletes23['col1'],rhoEstGmenores23['col1']],notch='True',bootstrap=1000)
plt.show()

plt.title('Pares Ap 1-2Mpc')
plt.boxplot([rhoEstPares23['col1'],rhoEstTripletes23['col1'],rhoEstGmenores23['col1']]
            ,notch='True',bootstrap=1000)
plt.show()

plt.title('Pares Ap 1-2Mpc')
plt.title('Pares Ap 1-2Mpc')
plt.boxplot([rhoEstPares12['col1'],rhoEstTripletes12['col1'],rhoEstGmenores12['col1']]
            ,notch='True',bootstrap=1000)
plt.show()

import seaborn
plt.title('Pares Ap 1-2Mpc')
plt.boxplot([rhoEstPares12['col1'],rhoEstTripletes12['col1'],rhoEstGmenores12['col1']]
            ,notch='True',bootstrap=1000)
plt.show()

plt.title('Ap 1-2Mpc')
plt.boxplot([rhoEstPares12['col1'],rhoEstTripletes12['col1'],rhoEstGmenores12['col1']]
            ,notch='True',bootstrap=1000)
plt.show()

plt.ylim?
plt.ylim((-2.8.))
plt.ylim([-2.8.])
plt.ylim([-2.,8.])
plt.ylim((-2.,8.))
plt.title('Ap 1-2Mpc')
plt.boxplot([rhoEstPares12['col1'],rhoEstTripletes12['col1'],rhoEstGmenores12['col1']]
            ,labels=['Pairs','Triplets','Minor Groups'],notch='True',bootstrap=1000)
plt.show()

plt.title('Ap 2-3Mpc')
plt.boxplot([rhoEstPares23['col1'],rhoEstTripletes23['col1'],rhoEstGmenores23['col1']]
            ,labels=['Pairs','Triplets','Minor Groups'],notch='True',bootstrap=1000)
plt.show()

plt.ylim((-2.,8.))
plt.title('Ap 3-4Mpc')
plt.boxplot([rhoEstPares34['col1'],rhoEstTripletes34['col1'],rhoEstGmenores34['col1']]
            ,labels=['Pairs','Triplets','Minor Groups'],notch='True',bootstrap=1000)
plt.show()

plt.ylim((-2.,8.))
plt.title('Ap 4-5Mpc')
plt.boxplot([rhoEstPares45['col1'],rhoEstTripletes45['col1'],rhoEstGmenores45['col1']]
            ,labels=['Pairs','Triplets','Minor Groups'],notch='True',bootstrap=1000)
plt.show()

plt.ylim((-2.,8.))
plt.title('Ap 1-2Mpc')
plt.boxplot([rhoEstPares12['col1'],rhoEstTripletes12['col1'],rhoEstGmenores12['col1']]
            ,labels=['Pairs','Triplets','Minor Groups'],notch='True',bootstrap=1000)
plt.show()

plt.ylim((-2.,8.))
plt.title('Ap 2-3Mpc')
plt.boxplot([rhoEstPares23['col1'],rhoEstTripletes23['col1'],rhoEstGmenores23['col1']]
            ,labels=['Pairs','Triplets','Minor Groups'],notch='True',bootstrap=1000)
plt.show()

plt.ylim((-2.,8.))
plt.title('Ap 3-4Mpc')
plt.boxplot([rhoEstPares34['col1'],rhoEstTripletes34['col1'],rhoEstGmenores34['col1']]
            ,labels=['Pairs','Triplets','Minor Groups'],notch='True',bootstrap=1000)
plt.show()

plt.ylim((-2.,8.))
plt.title('Ap 4-5Mpc')
plt.boxplot([rhoEstPares45['col1'],rhoEstTripletes45['col1'],rhoEstGmenores45['col1']]
            ,labels=['Pairs','Triplets','Minor Groups'],notch='True',bootstrap=1000)
plt.show()


##---(Wed Dec 13 15:44:25 2017)---
run imports
cd illustris/
run imports
print 'Random VPF. N=',N

x = np.random.uniform(0.,l_box,[N,])
y = np.random.uniform(0.,l_box,[N,])
z = np.random.uniform(0.,l_box,[N,])

print 'Creating KDTree...'
bounds = [l_box,l_box,l_box]
tree = spatial.cKDTree(data=np.column_stack([x,y,z]),boxsize=l_box)

apos,amass=[subgroups[x][(np.log10(subgroups['SubhaloMass'])>-1.)] for x in ['SubhaloPos','SubhaloMass']]
pos = Table(apos,names=['x','y','z'])

N = 25000
print 'N =',N
mass_column = Table.Column(amass,name='mass')
pos.add_column(mass_column)
pos.sort(['mass'])
pos = pos[-N:] #Selecciona los ultimos N objetos de la tabla

x = pos['x']
y = pos['y']
z = pos['z']

l_box = 75000. #kpc
mean_sep = ((l_box**3.) * 3. / (4. * np.pi * N))**(1./3.)

print 'Creating KDTree...'
bounds = [l_box,l_box,l_box]
tree = spatial.cKDTree(data=np.column_stack([x,y,z]),boxsize=l_box)

voidsFinal = ascii.read('voidsFinal_gxstracers.txt')

apos,amass=[subgroups[x][(np.log10(subgroups['SubhaloMass'])>-1.)] for x in ['SubhaloPos','SubhaloMass']]
pos = Table(apos,names=['x','y','z'])

N = 25000
print 'N =',N
mass_column = Table.Column(amass,name='mass')
pos.add_column(mass_column)
pos.sort(['mass'])
pos = pos[-N:] #Selecciona los ultimos N objetos de la tabla

x = pos['x']
y = pos['y']
z = pos['z']

l_box = 75000. #kpc
mean_sep = ((l_box**3.) * 3. / (4. * np.pi * N))**(1./3.)

print 'Creating KDTree...'
bounds = [l_box,l_box,l_box]
tree = spatial.cKDTree(data=np.column_stack([x,y,z]),boxsize=l_box)

voidsFinal = ascii.read('voidsFinal_gxstracers.txt')

Nran = 40000
#Nran = int( 10.* 4./3.*np.pi*10.**3 ) #Nran calculados para que tengan la misma densidad
			       	      #en la vpf_invoid y vpf_allsim
			              #Nran = Densidad * Volumen del Void[Mpc]
print 'Nran =',Nran

K=[]

ran_pts=[]

for nv in range(len(voidsFinal)):
 
	print nv,'/',len(voidsFinal)
	xv,yv,zv,rv = voidsFinal[nv]
 
	#Solo para contar objetos
	#idx = tree.query_ball_point([xv,yv,zv],rv) 
	#print len(idx) 
 
	for _ in range(Nran):
  
		theta = np.arccos(ran.uniform(-1.,1.)) # 0 < theta < Pi
		phi = ran.uniform(0.,2.*np.pi)         # 0 < phi < 2*Pi
		rho = (ran.uniform(0.,rv**3.))**(1./3) #
  
		xr = rho * np.sin(theta) * np.cos(phi)
		yr = rho * np.sin(theta) * np.sin(phi)
		zr = rho * np.cos(theta)
  
		ran_pts.append(rho/rv)
  
		rr = rv #Defino esto para que entre el while
		while rr+rho>rv:
			rr = ran.uniform(0.,rv)
			#print rr+rho, rv
		idx = tree.query_ball_point([xv+xr,yv+yr,zv+zr],rr)    
  
		K.append((int(len(idx)),rr/rv))		


K = np.reshape(K,(len(K),2))
cts = Table(K,names=['N','r / rv'])

runfile('/home/fdavilakurban/illustris/vpf0.py', wdir='/home/fdavilakurban/illustris')
runfile('/home/fdavilakurban/illustris/vpf_invoid.py', wdir='/home/fdavilakurban/illustris')