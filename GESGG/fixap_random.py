# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 12:09:25 2015

SIGMA5

@author: federico
"""

#%%
import numpy as np
import time
#import datetime

#%% --Parameters--

rpmin = .5 # Radio Min
rpmax = 2. # y Max del anillo 

dV = 1000. # Delta Velocidad[km/s] (Media Altura del Cilindro)

sample = 'todos'

#%% --Other Parameters--
Om = 0.3
c = 300000.
H_0 = 70.
pr = np.pi/180.

zmax = dV/H_0 # (dV/c)*(c/H_0)=dz[Mpc]
zmin = 0.0001


#%%

t1 = time.time()

from astropy.io import ascii
#from astropy.io import fits
from astropy.table import Table

galf  = '{}.txt'.format(sample) # data sample
if sample == 'todos': galf = 'SCG_centros_maskedallgxs_Compactos_clean.dat'
calf = 'random_DR10.dat'

fr = fast_reader={'parallel': True, 'use_fast_converter': True}
print 'Reading file: ', galf
gals = ascii.read(galf,delimiter=' ',fast_reader=fr)
print '--> nr of rows : ', len(gals)
print 'Reading file: ', calf
cals = ascii.read(calf,delimiter=' ',fast_reader=fr)
print '--> nr of rows : ', len(cals)

t2 = time.time()

print 'Time spent reading files: ',t2-t1,'s'

if cals.colnames[0]=='ra_1':
    cals.rename_column('ra_1','ra')
    cals.rename_column('dec_1','dec')

nt=len(gals)

#%% Pasaje de Unidades
t1=time.time()
gals['Xcent'] *= np.pi/180. # a radianes
gals['Ycent'] *= np.pi/180.
gals['zprom'] *= c/H_0      # a Mpc

cals['ra'] *= np.pi/180. # a radianes
cals['dec']*= np.pi/180.
cals['z']  *= c/H_0      # a Mpc

t2=time.time()
print t2-t1

#%% Calculo del Num de Bins para Crear la Grilla
t1=time.time()

a1=sorted(gals['Xcent'])
d1=sorted(gals['Ycent'])
z1=sorted(gals['zprom'])

rpmax1 = 10. # Aprox. 10 para optimizar 
zz = z1[0]*H_0/c # z min de la muestra
rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5)))) / (H_0*(Om**2)*(1.+zz)**2)

t2=time.time()

nbins=int((a1[-1]-a1[0])/(rpmax1/rz)),int((d1[-1]-d1[0])/(rpmax1/rz))

print 'Time spent calculating bins:',t2-t1,'s'

print 'Nbins=',nbins
#%% Renombro algunas columnas para usar en el hist2d
gals.rename_column('Xcent','ra')
gals.rename_column('Ycent','dec')
gals.rename_column('zprom','z')
gals.rename_column('name','groupID')

#%%
from astropy.table import Column
col = Column(name='groupID',length=len(cals)) # creo columna falsa para el hist2d
cals.add_column(col)

#%%
print 'Creating the coordenates matrices...'
t1=time.time()
import hist2drec_fede

c_data,H_d=hist2drec_fede.hist2d_fede(gals,nbins)#,plot=True,plttitle='Data')
print 'Data Matrix created'
c_cross,H_c=hist2drec_fede.hist2d_fede(cals,nbins)#,plot=True,plttitle='Cross') 
print 'Cross Matrix created'
t2=time.time()

print 'Time spent creating the matrices: ',(t2-t1)/60.,'m'

#%%
t1 = time.time()
iflag=0
jflag=0
cts=[]

for i in range(nbins[1]):
    #print i,'/',nbins[1]
    for j in range(nbins[0]):
        #print 'i,j:',i,j
        #if c_data[i,j,0,0] == 0.: continue # Revisar: no parece afectar
        for k in range(int(H_d[i,j])):
            
            z1 = c_data[i,j,k,2]+zmax
            z2 = c_data[i,j,k,2]-zmax
            zc = c_data[i,j,k,2]*H_0/c 
            cont = 0
            if  zc == 0.: 
                print i,j,k,'id:',c_data[i,j,k,3],'zc=0'                
                continue 
            for i1 in range(i-1,i+2):

                if i1==nbins[1]: 
                    i1 = 0
                    iflag = 1
                
                for j1 in range(j-1,j+2):
                    #print 'i1,j1',i1,j1
                    if j1==nbins[0]: 
                        j1 = 0
                        jflag = 1
                        
                    for k1 in range(int(H_c[i1,j1])):
                        #print i1,j1,k1
                        if c_cross[i1,j1,k1,2] >= z1 or c_cross[i1,j1,k1,2] <= z2: continue
                        if c_data[i,j,k,3] == c_cross[i1,j1,k1,3]:
                            #print i,j,k,'/',i1,j1,k1,':','Pertenecientes al mismo grupo:',c_data[i,j,k,3],c_cross[i1,j1,k1,3]
                            continue
                        zv = c_cross[i1,j1,k1,2]*H_0/c              
                        if  zv == 0.: 
                            print i,j,k,k1,'zv=0'
                            continue 

#-----------------------Calculo de la Dist Proy------------------------
                        zz = (zc+zv)/2.
                        rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5)))) / (H_0*(Om**2)*(1.+zz)**2)
                        if rz==0.: 
                            print 'rz=0'                            
                            continue
                        if rz <=0.1: print 'rz chico'    
                        da = abs(c_cross[i1,j1,k1,0]-c_data[i,j,k,0])
                    
                        if da > np.pi: da = 2.*np.pi-da
                        if da == 0.: continue
                            
                        dd = (np.sin(c_cross[i1,j1,k1,1])*np.sin(c_data[i,j,k,1])) + (np.cos(c_cross[i1,j1,k1,1])*np.cos(c_data[i,j,k,1]))*np.cos(da)
                        if dd==0.: print 'dd=0'        
                        if abs(dd) > 1.:                    
                                if dd < -1.: dd=-1.
                                if dd > 1.: dd=1.
                        dd = np.arccos(dd)    
                        rp = rz*dd
                        
                        if rp > rpmax or rp < rpmin: continue
                        cont += 1
                        
                        print 'rp =',rp,i,j,'/',nbins[1]-1

                        
                    if jflag == 1: 
                        j1=nbins[0]
                        jflag=0

                if iflag == 1: 
                    i1=nbins[1]
                    iflag=0
            
            if cont != 0: cts.append([cont,c_data[i,j,k,3]])    

t2=time.time()

print (t2-t1)/60.,'m'
#%%                
rho=[]
groupid=[]
for n,group_id in cts:
    #n=cts[i]
    rho.append(np.log10(n/(len(cts)*2*zmax*np.pi*(rpmax-rpmin)**2))) # Densidad sobre num de centros
    groupid.append(group_id)
#%%
dens_table = Table(data=[rho,groupid],names=('dens','group_id'))

#%%
ascii.write(dens_table,'densfixap_random_{0}_{1}_{2}.txt'.format(rpmin,rpmax,int(dV)),format='no_header')

        