# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 18:54:36 2015

@author: federico
"""
import numpy as np
import numpy.random as npr

#def bootstrap_resample(X, n=None):
#    """ Bootstrap resample an array_like
#    Parameters
#    ----------
#    X : array_like
#      data to resample
#    n : int, optional
#      length of resampled array, equal to len(X) if n==None
#    Results
#    -------
#    returns X_resamples
#    """
#    if n == None:
#        n = len(X)
#        
#    resample_i = np.floor(np.random.rand(n)*len(X)).astype(int)
#    X_resample = X[resample_i]
#    return X_resample
#    
#def bootstrap1(data, num_samples, statistic, alpha):
#    """Returns bootstrap estimate of 100.0*(1-alpha) CI for statistic."""
#    n = len(data)
#    idx = npr.randint(0, n, (num_samples, n))
#    samples = data[idx]
#    stat = np.sort(statistic(samples, 1))
#    return (stat[int((alpha/2.0)*num_samples)],
#            stat[int((1-alpha/2.0)*num_samples)])

def bootstrap(data, num_samples):
    n = len(data)
    idx = npr.randint(0, n, (num_samples, n))
    samples = data[idx]
    stat = (np.median(samples, 1))
    return np.std(stat)
