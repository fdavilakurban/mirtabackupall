# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 10:30:58 2015

@author: federico
"""

#%%
#===========================LECTURA DE DATOS======================================

rp = [] # El rp es el mismo para todos los estimadores, lo leo una sola vez

xi_nat = []
xi_dp  = []
xi_ls  = []
xi_ham = []

ejn_nat = []
ejn_dp  = []
ejn_ls  = []
ejn_ham = []

#=================Natural Estimator==============================================
readFile = open('../ProgramasGeorgi/CorrelationFunction/fede/output.pr.nat.txt','r')
line = readFile.read().split('\n')
for var in line:
    if var == '': break
    sepList = var.split()
    rp.append(float(sepList[0]))
    xi_nat.append(float(sepList[1]))
    ejn_nat.append(float(sepList[2]))

#=================DP Estimator==============================================
readFile = open('../ProgramasGeorgi/CorrelationFunction/fede/output.pr.dp.txt','r')
line = readFile.read().split('\n')
for var in line:
    if var == '': break
    sepList = var.split()
    xi_dp.append(float(sepList[1]))
    ejn_dp.append(float(sepList[2]))

#=================Hamilton Estimator==============================================
readFile = open('../ProgramasGeorgi/CorrelationFunction/fede/output.pr.ham.txt','r')
line = readFile.read().split('\n')
for var in line:
    if var == '': break
    sepList = var.split()
    xi_ham.append(float(sepList[1]))
    ejn_ham.append(float(sepList[2]))

#=================LS Estimator==============================================
readFile = open('../ProgramasGeorgi/CorrelationFunction/fede/output.pr.ls.txt','r')
line = readFile.read().split('\n')
for var in line:
    if var == '': break
    sepList = var.split()
    xi_ls.append(float(sepList[1]))
    ejn_ls.append(float(sepList[2]))

#%%

c1=[]
c2=[]
c3=[]
c1err=[]
c2err=[]
c3err=[]


for i in range(len(xi_nat)):
    c1.append(xi_nat[i]/xi_dp[i])
    c2.append(xi_nat[i]/xi_ls[i])
    c3.append(xi_nat[i]/xi_ham[i])
    
    c1err.append(c1[i]*(ejn_nat[i]/xi_nat[i] + ejn_dp[i]/xi_dp[i]))
    c2err.append(c2[i]*(ejn_nat[i]/xi_nat[i] + ejn_ls[i]/xi_ls[i]))
    c3err.append(c3[i]*(ejn_nat[i]/xi_nat[i] + ejn_ham[i]/xi_ham[i]))

#%%
import matplotlib.pyplot as plt
from pylab import rcParams

plt.close()
rcParams['figure.figsize'] = 15, 10

plt.errorbar(rp, c1, yerr=c1err, color='r', fmt='')
plt.scatter(rp,c1,color='r',s=25,label='Nat/DP')

plt.errorbar(rp, c2, yerr=c2err, color='b', fmt='')
plt.scatter(rp,c2,color='b',s=25,label='Nat/LS')

plt.errorbar(rp, c3, yerr=c3err, color='gray', fmt='')
plt.scatter(rp,c3,color='gray',s=25,label='Nat/Ham')



plt.grid(True)
plt.xlabel('$\sigma$',fontsize='x-large')
plt.ylabel('$\omega_1(\sigma)/\omega_2(\sigma)$',fontsize='xx-large')
plt.title('Ratio All Estimators',fontsize='x-large')
plt.legend(fancybox=True,shadow=True,frameon=True,fontsize=20)

plt.xlim(10.**-1., 10.**1.1) 
#plt.ylim(10**0.8, 10.**3.2)
plt.yscale('log')
plt.xscale('log')

#plt.savefig('graf_coc_ws.pdf')
plt.show()
