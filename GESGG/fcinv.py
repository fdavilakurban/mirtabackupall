# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 09:59:52 2015

Inversion de la func de correlacion

@author: federico
"""

import time

t1=time.time()

import numpy as np  #
nbin = 10      # Borrar cuando se integre al programa principal

sample = 'paresc'

for est in ['nat','dp','ham','ls']:
        
    s = []
    w = []
    xi = np.zeros((nbin),'float')  # Inicializo un contador para el numero de objetos
    
    l = np.zeros((nbin,10),'int')  
    rp = np.zeros((nbin,10),'float')  
    wm = np.zeros((nbin,10),'float')  
    wjn = np.zeros((nbin,10),'float')  
    
    xijn = np.zeros((nbin,10),'float') 
    
    xm = np.zeros((nbin),'float')
    ejn = np.zeros((nbin),'float')
    
    
    # Archivos    output.pr.xxx.txt
    #             output.xxx.rev.txt
    #             outputjn.xxx.txt
    #
    # Estimadores xxx: 'nat', 'dp', 'ls', 'ham'
    
    inputFile_nat = open('output_hist2d.pr.{0}_{1}'.format(est,sample),'r')
    outputFile_nat = open('output_hist2d.{0}_{1}.rev.txt'.format(est,sample),'w')
    inputjnFile_nat = open('outputjn.{0}_{1}'.format(est,sample),'r')
    
    line = inputFile_nat.read().split('\n')
    for var in line:
        
        if var == '': break
        
        sepList = var.split()
        
        s.append(float(sepList[0]))
        w.append(float(sepList[1]))
    
    
    for i in range(nbin):
        
        for j in range(nbin-1):  # Abajo hay operaciones con j+1
                
                if j >= i:
                    p1 = w[j+1]-w[j]
                    p2 = s[j+1]-s[j]
                    p3 = s[j+1]+np.sqrt(s[j+1]**2-s[i]**2)
                    p4 = s[j]+np.sqrt(s[j]**2-s[i]**2)
    
                    xi[i]=xi[i]+(p1/p2)*(np.log(p3/p4)) 
                                    
        xi[i] = xi[i]*(-1./np.pi)
    
    
    line = inputjnFile_nat.read().split('\n')
    i=0
    j=0
    for var in line:
        
        if var == '': break
        
        sepList = var.split()
        
        l[i,j]   = float(sepList[0])
        rp[i,j]  = float(sepList[1])             
        wm[i,j]  = float(sepList[2])
        wjn[i,j] = float(sepList[3])
    
        j=j+1
        if j == 10:  # Esto lo hago para que se parezca a un doble lazo 'do' en Fortran.
            j=0      # Probablemente no sea lo más eficiente. Revisar en el futuro.
            i=i+1
            
    for k in range(10):
        
        for i in range(nbin):
        
            for j in range(nbin-1):  # Abajo hay operaciones con j+1
                
                 if j >= i:
                    p1j = wjn[j+1,k]-wjn[j,k]
                    p2j = rp[j+1,k]-rp[j,k]
                    p3j = rp[j+1,k]+np.sqrt(rp[j+1,k]**2-rp[i,k]**2)
                    p4j = rp[j,k]+np.sqrt(rp[j,k]**2-rp[i,k]**2)
    
                    xijn[i,k] = xijn[i,k]+(p1j/p2j)*(np.log(p3j/p4j)) 
                                    
            xijn[i,k] = xijn[i,k]*(-1./np.pi)
    
    for i in range(nbin):
        for k in range(10):
            xm[i] = xm[i]+xijn[i,k]
        xm[i] = xm[i]/10.
        
    for i in range(nbin):
        for k in range(10):
            ejn[i] = ejn[i]+(xijn[i,k]-xm[i])**2
        ejn[i] = np.sqrt(ejn[i]/10.)
    
        if i != (nbin-1): outputFile_nat.write(str(s[i])+' '+str(xi[i])+' '+str(ejn[i])+'\n')
    
    inputFile_nat.close()
    outputFile_nat.close()
    inputjnFile_nat.close()
    
    t2=time.time()
    
    print t2-t1