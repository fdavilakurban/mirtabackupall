# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 21:24:58 2015

Histogramas

@author: federico
"""
#%%
from astropy.io import ascii
import matplotlib.pyplot as plt
#%%

#stat = 'sigma'
stat = 'fixap'

#sample1 = 'paresc'
sample1 = 'pares'
sample2 = 'tripletes'
sample3 = 'gmenores'
#sample4 = 'gm6'

if stat == 'sigma':
    galf1 = 'sigma5_{}.txt'.format(sample1)
    galf2 = 'sigma5_{}.txt'.format(sample2)
    galf3 = 'sigma5_{}.txt'.format(sample3)
    #galf4 = 'sigma5_{}.txt'.format(sample4)
    
rpmin = 4. # Radio Min
rpmax = 5. # y Max del anillo 
dV = 1000. # Delta Velocidad[km/s] (Media Altura del Cilindro)
if stat == 'fixap':
	galf1 = 'fixap_{}_{}-{}-{}_est.txt'.format(sample1,rpmin,rpmax,int(dV))
	galf2 = 'fixap_{}_{}-{}-{}_est.txt'.format(sample2,rpmin,rpmax,int(dV))
	galf3 = 'fixap_{}_{}-{}-{}_est.txt'.format(sample3,rpmin,rpmax,int(dV))
	
	
	
	
#galf1='random_todos.txt'
#galf2='SCG_centros_maskedallgxs_Compactos_clean.dat'
#galf3='fixap_gmenores_0.5-1.0-1000.txt'

fr = fast_reader={'parallel': True, 'use_fast_converter': True}
gals1 = ascii.read(galf1,delimiter=' ',fast_reader=fr,format='no_header',fill_values=('nan','-3.'))
gals2 = ascii.read(galf2,delimiter=' ',fast_reader=fr,format='no_header',fill_values=('nan','-3.'))
gals3 = ascii.read(galf3,delimiter=' ',fast_reader=fr,format='no_header',fill_values=('nan','-3.'))
#gals4 = ascii.read(galf4,delimiter=' ',fast_reader=fr,format='no_header',fill_values=('nan','-3.'))
#%%
#gals1=gals1[gals1['col1']<=.9]
#gals2=gals2[gals2['col1']<=.9]
#gals3=gals3[gals3['col1']<=.9]
#gals4=gals4[gals4['col1']<=.3]

#%%
plt.close()
bins=10
rwidth=.25
plt.hist(gals1['col1'], bins=50, histtype='step',linewidth=1.8,linestyle='solid', normed=True, color='k', alpha=1,label='Pairs')#, rwidth=rwidth)
plt.hist(gals2['col1'], bins=45, histtype='step',linewidth=1.8,linestyle='solid', normed=True, color='blue', alpha=1,label='Triplets')#, rwidth=rwidth)
plt.hist(gals3['col1'], bins=34, histtype='step',linewidth=1.8,linestyle='solid', normed=True, color='red', alpha=1,label='Minor Groups')#, rwidth=rwidth)
#plt.hist(gals4['col1'], bins=16, histtype='step',linewidth=1.8,linestyle='solid', normed=False, color='g', alpha=1,label=sample4)#, rwidth=rwidth)

plt.xlim(-5.,20.)
#plt.gca().set_xscale("log")

if stat == 'fixap':
	plt.title('${}Mpc\leq r_p\leq {}Mpc\,;\, \Delta V\leq {}Km/s$'.format(rpmin,rpmax,dV),fontsize=18)
if stat == 'sigma':
	plt.xlabel('$\log (\Sigma_{5})$',fontsize=18)
if stat == 'fixap':
	plt.xlabel('$(N-Nran)/Nran$',fontsize=18)
#plt.xlabel('$n/V$',fontsize=18)
plt.ylabel('Normalized Distribution',fontsize=13)
plt.tight_layout()
plt.legend(fancybox=True,frameon=True,shadow=True,loc=1)
plt.show()    
if stat == 'sigma':
	plt.savefig('Plots/sigma5.pdf')
if stat == 'fixap':
	#cd Plots
	plt.savefig('Plots/fixap_{}-{}-{}.pdf'.format(rpmin,rpmax,dV))
	
#%%
from scipy import stats

ks,pv=stats.ks_2samp(gals1['col1'],gals2['col1'])
print '{0}, {1}:'.format(sample1,sample2),'ks=',ks,';pv=',pv

ks,pv=stats.ks_2samp(gals1['col1'],gals3['col1'])
print '{0}, {1}:'.format(sample1,sample3),'ks=,',ks,';pv=',pv

#ks,pv=stats.ks_2samp(gals1['col1'],gals4['col1'])
#print '{0}, {1}:'.format(sample1,sample4),'ks=',ks,';pv=',pv

ks,pv=stats.ks_2samp(gals2['col1'],gals3['col1'])
print '{0}, {1}:'.format(sample2,sample3),'ks=,',ks,';pv=',pv

#ks,pv=stats.ks_2samp(gals2['col1'],gals4['col1'])
#print '{0}, {1}:'.format(sample2,sample4),'ks=',ks,';pv=',pv

#ks,pv=stats.ks_2samp(gals3['col1'],gals4['col1'])
#print '{0}, {1}:'.format(sample3,sample4),'ks=',ks,';pv=',pv

