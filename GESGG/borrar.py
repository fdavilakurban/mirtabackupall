from astropy.io import ascii

galf = 'SCG_centros_maskedallgxs_Compactos.dat'

cols = ['name','Xcent','Ycent','zprom','dVmax','sigma','rpmax(fromcent)','sumr90','Rminkpc','S','Mr13','Nm','nspec']
fr = fast_reader={'parallel': True, 'use_fast_converter': True}
#print 'Reading file: ', galf
gals = ascii.read(galf,delimiter=' ',fast_reader=fr)
#print '--> nr of rows : ', len(gals)
#%%

gpar = gals[gals['Nm']==2]
gtri = gals[gals['Nm']==3]
gm = gals[gals['Nm']>=4]

#%% 

ascii.write(gpar,'pares.txt')
ascii.write(gtri,'tripletes.txt')
ascii.write(gm,'gmenores.txt')