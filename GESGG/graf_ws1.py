# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 10:30:58 2015

@author: federico
"""

#%%
from astropy.io import ascii

"""
===========================LECTURA DE DATOS======================================
"""
sample = 'gmenores'

#=================Natural Estimator==============================================
nat=ascii.read('output_hist2d.pr.nat_{}'.format(sample))

#=================DP Estimator==============================================
dp=ascii.read('output_hist2d.pr.dp_{}'.format(sample))

#=================Hamilton Estimator==============================================
ham=ascii.read('output_hist2d.pr.ham_{}'.format(sample))

#=================LS Estimator==============================================
ls=ascii.read('output_hist2d.pr.ls_{}'.format(sample))
#%%
#nat.remove_rows(slice(0,2))
#ham.remove_rows(slice(0,2))
#ls.remove_rows(slice(0,2))
#dp.remove_rows(slice(0,2))

nat.remove_row(0)
ham.remove_row(0)
ls.remove_row(0)
dp.remove_row(0)
#%%
import matplotlib.pyplot as plt
from pylab import rcParams
plt.close()
#rcParams['figure.figsize'] = 15, 10

plt.errorbar(nat['col1'], nat['col2'], yerr=nat['col3'], color='k', fmt='')
plt.scatter(nat['col1'], nat['col2'],color='k',s=25,label='Natural')

plt.errorbar(dp['col1'], dp['col2'], yerr=dp['col3'], color='r', fmt='')
plt.scatter(dp['col1'], dp['col2'],color='r',s=25,label='Davis-Peebles')

plt.errorbar(ham['col1'], ham['col2'], yerr=ham['col3'], color='b', fmt='')
plt.scatter(ham['col1'], ham['col2'],color='b',s=25,label='Hamilton')

plt.errorbar(ls['col1'], ls['col2'], yerr=ls['col3'], color='g', fmt='')
plt.scatter(ls['col1'], ls['col2'],color='g',s=25,label='Landy-Szalay')




plt.grid()
plt.xlabel('$\sigma\,(Mpc\,h^{-1})$',fontsize='x-large')
plt.ylabel('$\omega(\sigma)$',fontsize='xx-large')
#plt.title('FC-{0} Todos los Estimadores'.format(sample),fontsize='x-large')
plt.legend(fancybox=True,shadow=True,frameon=True,fontsize=20)

plt.xlim(10.**-0.5, 10.**1.4) 
plt.ylim(10**0.5, 10.**2)
plt.yscale('log')
plt.xscale('log')
plt.tight_layout()
#plt.savefig('graf_ws_{}.pdf'.format(sample))
plt.show()    