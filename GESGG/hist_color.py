# -*- coding: utf-8 -*-
"""
Created on Mon Dec 14 18:09:49 2015

@author: federico
"""
from astropy.io import ascii
import matplotlib.pyplot as plt
#%%
galf = 'SCG_vecinos_final_masked_Compactos_gr_C_rpmax.dat'

fr = fast_reader={'parallel': True, 'use_fast_converter': True}
print 'Reading file: ', galf
gals = ascii.read(galf,delimiter=' ',fast_reader=fr,fill_values=('--'))
print '--> nr of rows : ', len(gals)
#%%
gpar = gals[gals['Nm']==2]
gpar = gpar[(2.*gpar['rpmax'])<=100.]

gtri = gals[gals['Nm']==3]
gm = gals[gals['Nm']>=4]

print 'nr de pares, tri, gm:',len(gpar),len(gtri),len(gm)
#%%
gpar_b=gpar[gpar['gr']<.75]
gpar_r=gpar[gpar['gr']>.75]

gtri_b=gtri[gtri['gr']<.75]
gtri_r=gtri[gtri['gr']>.75]

gm_b=gm[gm['gr']<.75]
gm_r=gm[gm['gr']>.75]

print 'Fraccion de Pares Azules:',float(len(gpar_b))/len(gpar)
print 'Fraccion de Tripletes Azules:',float(len(gtri_b))/len(gtri)
print 'Fraccion de G. Menores Azules:',float(len(gm_b))/len(gm)
#print ''
#print 'Fraccion de Pares Rojas:',float(len(gpar_r))/len(gpar)
#print 'Fraccion de Tripletes Rojas:',float(len(gtri_r))/len(gtri)
#print 'Fraccion de G. Menores Rojas:',float(len(gm_r))/len(gm)
#%%
plt.close()
bins=10
rwidth=.25
plt.hist(gpar['gr'], bins=150, histtype='step',linewidth=1.8,linestyle='solid', normed=True, color='black', alpha=1,label='Pares')#, rwidth=rwidth)
plt.hist(gtri['gr'], bins=55, histtype='step',linewidth=1.5,linestyle='solid', normed=True, color='b', alpha=1,label='Tripletes')#, rwidth=rwidth)
plt.hist(gm['gr'], bins=30, histtype='step',linewidth=1.2,linestyle='solid', normed=True, color='r', alpha=1,label='Grupos Menores')#, rwidth=rwidth)

#plt.title('',fontsize=18)
plt.xlabel('$g-r$',fontsize=18)
plt.ylabel('Distr. Normalizada',fontsize=13)
plt.xlim(-0., 1.5)
plt.tight_layout()
plt.legend(fancybox=True,frameon=True,shadow=True,loc=2)
plt.show()    
plt.savefig('histcolor.pdf')