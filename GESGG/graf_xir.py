# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 10:01:48 2015

Grafico: Xi(r)

@author: federico
"""
#def plot_xir(sample1):
#%%
from astropy.io import ascii

"""
===========================LECTURA DE DATOS======================================
"""
sample = 'gmenores'

#=================Natural Estimator==============================================
nat=ascii.read('output_hist2d.nat_{}.rev.txt'.format(sample))

#=================DP Estimator==============================================
dp=ascii.read('output_hist2d.dp_{}.rev.txt'.format(sample))

#=================Hamilton Estimator==============================================
ham=ascii.read('output_hist2d.ham_{}.rev.txt'.format(sample))

#=================LS Estimator==============================================
ls=ascii.read('output_hist2d.ls_{}.rev.txt'.format(sample))
#%%
nat.remove_row(0)
ham.remove_rows(slice(1))
ls.remove_row(0)
dp.remove_row(0)
       
#%%

import matplotlib.pyplot as plt
from pylab import rcParams

plt.close()

#rcParams['figure.figsize'] = 15, 10



plt.errorbar(nat['col1'], nat['col2'], yerr=nat['col3'], color='k', fmt='')
plt.scatter(nat['col1'], nat['col2'],color='k',s=25,label='Natural')

plt.errorbar(dp['col1'], dp['col2'], yerr=dp['col3'], color='r', fmt='')
plt.scatter(dp['col1'], dp['col2'],color='r',s=25,label='Davis-Peebles')

plt.errorbar(ham['col1'], ham['col2'], yerr=ham['col3'], color='b', fmt='')
plt.scatter(ham['col1'], ham['col2'],color='b',s=25,label='Hamilton')

plt.errorbar(ls['col1'], ls['col2'], yerr=ls['col3'], color='g', fmt='')
plt.scatter(ls['col1'], ls['col2'],color='g',s=25,label='Landy-Szalay')



plt.grid()
plt.xlabel('$r\,(Mpc\,h^{-1})$',fontsize='x-large')
plt.ylabel('$\\xi(r)$',fontsize='xx-large')
#plt.title('CF Rev. All Estimators ({})'.format(sample),fontsize='x-large')
plt.legend(fancybox=True,shadow=True,frameon=True,fontsize=20)

plt.xlim(10.**-0.5, 10.**1.2) 
plt.ylim(10**-1, 10.**2)
plt.yscale('log')
plt.xscale('log')
plt.tight_layout()
#plt.savefig('graf_xir_{}.pdf'.format(sample))
plt.show()    
