# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 19:02:07 2015

@author: federico
"""

#%%
import numpy as np
import time
#import datetime

#%%
timeFile = open('../ProgramasGeorgi/CorrelationFunction/fede/timertotal_hist2d.txt','a')

t0 = time.time()


Om = 0.3
c = 300000.
H_0 = 70.
pr = np.pi/180.

nbdp = 10
nbdv = 10

rmax = np.log10(20.)
rmin = np.log10(0.15)

zmax = 60.
zmin = 0.0001

dbin = (rmax-rmin)/float(nbdp)
zbin = (zmax-zmin)/float(nbdv) 

rpf = np.zeros((nbdp),'float')

fdd = np.zeros((nbdp,nbdv),'int') #Array bidimensional (ceros, enteros) 
frr = np.zeros((nbdp,nbdv),'int') #Array bidimensional (ceros, enteros) 
fdr = np.zeros((nbdp,nbdv),'int') #Array bidimensional (ceros, enteros) 

drjn = np.zeros((10,nbdp,nbdv),'int') # Array tridimensional 10 x nbdp x nbdv ceros
ddjn = np.zeros((10,nbdp,nbdv),'int') # Array tridimensional 10 x nbdp x nbdv ceros
rrjn = np.zeros((10,nbdp,nbdv),'int') # Array tridimensional 10 x nbdp x nbdv ceros

njndd = np.zeros((10),'int')  # Inicializo un contador para el numero de objetos
njnrr = np.zeros((10),'int')  # que tienen vecinos para cada jackknife

nbins = 15 # Para el hist2d 




#%%

t1 = time.time()

from astropy.io import ascii


galf  = '../ProgramasGeorgi/CorrelationFunction/fede/adz.targets' # data sample
rcalf  = '../ProgramasGeorgi/CorrelationFunction/fede/adz.random'  # random cross sample
calf  = '../ProgramasGeorgi/CorrelationFunction/fede/adz.tracers' # cross sample
rgalf  = '../ProgramasGeorgi/CorrelationFunction/fede/adz.targetrandoms' # random target sample

# READ INPUT DATA (fast!) into astropy tables
cols = ['ra','dec','z']#,'apm','abm','zmin','zmax','wei']
fr = fast_reader={'parallel': True, 'use_fast_converter': True}
print 'Reading file: ', galf
gals = ascii.read(galf,format='no_header',delimiter=' ',fast_reader=fr,names=cols)
print '--> nr of rows : ', len(gals)
print 'Reading file: ', rcalf
rcals = ascii.read(rcalf,format='no_header',delimiter=' ',fast_reader=fr,names=cols)
print '--> nr of rows : ', len(rcals)
print 'Reading file: ', calf
cals = ascii.read(calf,format='no_header',delimiter=' ',fast_reader=fr,names=cols)
print '--> nr of rows : ', len(cals)
print 'Reading file: ', rgalf
rgals = ascii.read(rgalf,format='no_header',delimiter=' ',fast_reader=fr,names=cols)
print '--> nr of rows : ', len(rgals)

t2 = time.time()

print 'Time spent reading files: ',t2-t1,'s'

nt=len(gals)
nr=len(rgals)
nr4=len(rcals)
nd4=len(cals)

#%%

print 'Creating the coordenates matrices'
t1=time.time()
import hist2d_fede

c_data,H_d=hist2d_fede.hist2d_fede(gals,nbins)#,plot=True,plttitle='Data')
c_cross,H_c=hist2d_fede.hist2d_fede(cals,nbins)#,plot=True,plttitle='Cross')
c_rdata,H_rd=hist2d_fede.hist2d_fede(rgals,nbins)#,plot=True,plttitle='Random (Data)')
c_rcross,H_rc=hist2d_fede.hist2d_fede(rcals,nbins)#,plot=True,plttitle='Random (Cross)')
t2=time.time()
print 'Time spent creating the matrices: ',t2-t1,'s'

#%%

# =====================================================================
# ************* Calculo de data-cross (target-tracer) ******************
# =====================================================================

t1 = time.time()
ntt = 0
iflag=0
jflag=0

for i in range(nbins):
    
    for j in range(nbins):
        
        print 'DD',i,j
        if c_data[i,j,0,0] == 0.: continue # Revisar: no parece afectar
        for k in range(int(H_d[i,j])):
        
            z1 = c_data[i,j,k,2]+60. 
            z2 = c_data[i,j,k,2]-60.
            zc = c_data[i,j,k,2]*H_0/c 
            
            ntv = 0            
            
            for i1 in range(i-1,i+2):

                if i1==nbins: 
                    i1 = 0
                    iflag = 1
                
                for j1 in range(j-1,j+2):
                    
                    if j1==nbins: 
                        j1 = 0
                        jflag = 1
                        
                    for k1 in range(int(H_c[i1,j1])):
                        #print i1,j1,k1
                        if c_cross[i1,j1,k1,2] >= z1 or c_cross[i1,j1,k1,2] <= z2: continue
                        zv = c_cross[i1,j1,k1,2]*H_0/c              
        
#-----------------------Calculo de la Dist Proy------------------------
                        zz = (zc+zv)/2.
                        rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5)))) / (H_0*(Om**2)*(1.+zz)**2)
                
                        da = abs(c_cross[i1,j1,k1,0]-c_data[i,j,k,0])
                    
                        if da > np.pi: da = 2.*np.pi-da
                        if da == 0.: continue
                            
                        dd = (np.sin(c_cross[i1,j1,k1,1])*np.sin(c_data[i,j,k,1])) + (np.cos(c_cross[i1,j1,k1,1])*np.cos(c_data[i,j,k,1]))*np.cos(da)
                                
                        if abs(dd) > 1.:                    
                                if dd < -1.: dd=-1.
                                if dd > 1.: dd=1.
                        dd = np.arccos(dd)    
                        
                        rp = rz*dd
                        rpl = np.log10(rp)
                
                        if rpl >= rmax or rpl <= rmin: continue                    
#---------------------------------------------------------------------
# --------------------  Calculo de la Dist Radial --------------------
        
                        difd = abs(c_data[i,j,k,2]-c_cross[i1,j1,k1,2])
                        didl = difd
                                
                        if didl >= zmax or didl <= zmin: continue
#---------------------------------------------------------------------
                        
# --------------------  Bineado en rp y deltaV -----------------------         
        
                        ind = int((rpl-rmin)/dbin)  # ind y inz toman valores
                        inz = int((didl-zmin)/zbin) # entre 0 y 9
                                             
                        fdd[ind,inz] = fdd[ind,inz]+1 
                        ntv = ntv+1
                        
                    if jflag == 1: 
                        j1=nbins
                        jflag=0
#---------------------------------------------------------------------
                if iflag == 1: 
                    i1=nbins
                    iflag=0                    
            if ntv > 0: ntt = ntt+1


t2 = time.time()

print 'Data-cross time elapsed:',(t2-t1)/60.,'m'

#%%

# =====================================================================
# ************* Calculo de Randata-Rancross ******************
# =====================================================================

t1 = time.time()
nrr = 0
iflag=0
jflag=0

for i in range(nbins):
    
    for j in range(nbins):
        
        print 'RR',i,j
        if c_rdata[i,j,0,0] == 0.: continue # Revisar: no parece afectar
        for k in range(int(H_rd[i,j])):
        
            z1 = c_rdata[i,j,k,2]+60. 
            z2 = c_rdata[i,j,k,2]-60.
            zc = c_rdata[i,j,k,2]*H_0/c 
            
            nrv = 0            
            
            for i1 in range(i-1,i+2):

                if i1==nbins: 
                    i1 = 0
                    iflag = 1
                
                for j1 in range(j-1,j+2):
                    
                    if j1==nbins: 
                        j1 = 0
                        jflag = 1
                        
                    for k1 in range(int(H_rc[i1,j1])):
                        if c_rcross[i1,j1,k1,2] >= z1 or c_rcross[i1,j1,k1,2] <= z2: continue
                        zv = c_rcross[i1,j1,k1,2]*H_0/c              
        
#-----------------------Calculo de la Dist Proy------------------------
                        zz = (zc+zv)/2.
                        rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5)))) / (H_0*(Om**2)*(1.+zz)**2)
                
                        da = abs(c_rcross[i1,j1,k1,0]-c_rdata[i,j,k,0])
                    
                        if da > np.pi: da = 2.*np.pi-da
                        if da == 0.: continue
                            
                        dd = (np.sin(c_rcross[i1,j1,k1,1])*np.sin(c_rdata[i,j,k,1])) + (np.cos(c_rcross[i1,j1,k1,1])*np.cos(c_rdata[i,j,k,1]))*np.cos(da)
                                
                        if abs(dd) > 1.:                    
                                if dd < -1.: dd=-1.
                                if dd > 1.: dd=1.
                        dd = np.arccos(dd)    
                        
                        rp = rz*dd
                        rpl = np.log10(rp)
                
                        if rpl >= rmax or rpl <= rmin: continue                    
#---------------------------------------------------------------------
# --------------------  Calculo de la Dist Radial --------------------
        
                        difd = abs(c_rdata[i,j,k,2]-c_rcross[i1,j1,k1,2])
                        didl = difd
                                
                        if didl >= zmax or didl <= zmin: continue
#---------------------------------------------------------------------
                        
# --------------------  Bineado en rp y deltaV -----------------------         
        
                        ind = int((rpl-rmin)/dbin)  # ind y inz toman valores
                        inz = int((didl-zmin)/zbin) # entre 0 y 9
                                             
                        frr[ind,inz] = frr[ind,inz]+1 
                        nrv = nrv+1
                        
                    if jflag == 1: 
                        j1=nbins
                        jflag=0
#---------------------------------------------------------------------
                if iflag == 1: 
                    i1=nbins
                    iflag=0
                    
            if nrv > 0: nrr = nrr+1

t2 = time.time()

print 'RanData-RanCross time elapsed:',(t2-t1)/60.,'m'

#%%

# =====================================================================
# ************* Calculo de Data-Rancross ******************
# =====================================================================

t1 = time.time()
ntr = 0
iflag=0
jflag=0

for i in range(nbins):
    
    for j in range(nbins):
        
        print 'DR',i,j
        if c_data[i,j,0,0] == 0.: continue # Revisar: no parece afectar
        for k in range(int(H_d[i,j])):
        
            z1 = c_data[i,j,k,2]+60. 
            z2 = c_data[i,j,k,2]-60.
            zc = c_data[i,j,k,2]*H_0/c 
            
            ntrv = 0            
            
            for i1 in range(i-1,i+2):

                if i1==nbins: 
                    i1 = 0
                    iflag = 1
                
                for j1 in range(j-1,j+2):
                    
                    if j1==nbins: 
                        j1 = 0
                        jflag = 1
                        
                    for k1 in range(int(H_rc[i1,j1])):
                        if c_rcross[i1,j1,k1,2] >= z1 or c_rcross[i1,j1,k1,2] <= z2: continue
                        zv = c_rcross[i1,j1,k1,2]*H_0/c              
        
#-----------------------Calculo de la Dist Proy------------------------
                        zz = (zc+zv)/2.
                        rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5)))) / (H_0*(Om**2)*(1.+zz)**2)
                
                        da = abs(c_rcross[i1,j1,k1,0]-c_data[i,j,k,0])
                    
                        if da > np.pi: da = 2.*np.pi-da
                        if da == 0.: continue
                            
                        dd = (np.sin(c_rcross[i1,j1,k1,1])*np.sin(c_data[i,j,k,1])) + (np.cos(c_rcross[i1,j1,k1,1])*np.cos(c_data[i,j,k,1]))*np.cos(da)
                                
                        if abs(dd) > 1.:                    
                                if dd < -1.: dd=-1.
                                if dd > 1.: dd=1.
                        dd = np.arccos(dd)    
                        
                        rp = rz*dd
                        rpl = np.log10(rp)
                
                        if rpl >= rmax or rpl <= rmin: continue                    
#---------------------------------------------------------------------
# --------------------  Calculo de la Dist Radial --------------------
        
                        difd = abs(c_data[i,j,k,2]-c_rcross[i1,j1,k1,2])
                        didl = difd
                                
                        if didl >= zmax or didl <= zmin: continue
#---------------------------------------------------------------------
                        
# --------------------  Bineado en rp y deltaV -----------------------         
        
                        ind = int((rpl-rmin)/dbin)  # ind y inz toman valores
                        inz = int((didl-zmin)/zbin) # entre 0 y 9
                                             
                        fdr[ind,inz] = fdr[ind,inz]+1 
                        ntrv = ntrv+1
                        
                    if jflag == 1: 
                        j1=nbins
                        jflag=0
#---------------------------------------------------------------------
                if iflag == 1: 
                    i1=nbins
                    iflag=0
                    
            if ntrv > 0: ntr = ntr+1

t2 = time.time()

print 'Data-RanCross time elapsed:',(t2-t1)/60.,'m'

#%%

# ======================================================================
# -------------- Jackknife  ---------------------
# ======================================================================


ejn_nat = np.zeros((nbdp),'float')
ejn_dp = np.zeros((nbdp),'float')
ejn_ham = np.zeros((nbdp),'float')
ejn_ls = np.zeros((nbdp),'float')

#%%
t1 = time.time()

# ======================================================================
# -------------- Calculo la Funcion de Correlacion ---------------------
# ======================================================================

xi_nat = np.zeros((10),'float')
xi_dp  = np.zeros((10),'float')
xi_ham = np.zeros((10),'float')
xi_ls  = np.zeros((10),'float')

w_nat = np.zeros((nbdp,nbdv),'float')
w_dp  = np.zeros((nbdp,nbdv),'float')
w_ham = np.zeros((nbdp,nbdv),'float')
w_ls  = np.zeros((nbdp,nbdv),'float')

dn1 = float(nrr)*float(nr4)
dn2 = float(ntt)*float(nd4)
rnorm = dn1/dn2

outputfsFile = open('../ProgramasGeorgi/CorrelationFunction/fede/output_hist2d.fs.txt','w')

outputprFile_nat = open('../ProgramasGeorgi/CorrelationFunction/fede/output_hist2d.pr.nat','w')
outputprFile_dp  = open('../ProgramasGeorgi/CorrelationFunction/fede/output_hist2d.pr.dp','w')
outputprFile_ham = open('../ProgramasGeorgi/CorrelationFunction/fede/output_hist2d.pr.ham','w')
outputprFile_ls  = open('../ProgramasGeorgi/CorrelationFunction/fede/output_hist2d.pr.ls','w')

for i in range(nbdp):
    rpf[i] = rmin+dbin/2.*(2.*float(i+1)-1.) # Codigo en Fortran dice float(i)
    rpf[i] = 10.**rpf[i]
            
    for j in range(nbdv):
            
        if fdd[i,j] == 0. or frr[i,j] == 0. or fdr[i,j] == 0.: continue
        ww_nat = float(fdd[i,j])/float(frr[i,j])
        ww_dp  = float(fdd[i,j])/float(fdr[i,j])
        ww_ham = float(fdd[i,j])*float(frr[i,j])/(float(fdr[i,j])**2)
        ww_ls  = float(fdr[i,j])/float(frr[i,j])
 
        xi_nat[i] = xi_nat[i]+ww_nat*rnorm**2-1.
        xi_dp[i]  = xi_dp[i]+ww_dp*rnorm-1.
        xi_ham[i] = xi_ham[i]+ww_ham-1.
        xi_ls[i]  = xi_ls[i]+ww_nat*rnorm**2-2.*rnorm*ww_ls+1.
        
        outputfsFile.write(str(i)+' '+str(j)+' '+str(fdd[i,j])+' '+str(frr[i,j])+' '+str(fdr[i,j])+'\n')
        
    xi_nat[i] = 2.*xi_nat[i]*zbin
    xi_dp[i]  = 2.*xi_dp[i]*zbin
    xi_ham[i] = 2.*xi_ham[i]*zbin
    xi_ls[i]  = 2.*xi_ls[i]*zbin
    
    outputprFile_nat.write(str(rp)+' '+str(xi_nat[i])+' '+str(ejn_nat[i])+'\n')
    outputprFile_dp.write(str(rp)+' '+str(xi_dp[i])+' '+str(ejn_dp[i])+'\n')
    outputprFile_ham.write(str(rp)+' '+str(xi_ham[i])+' '+str(ejn_ham[i])+'\n')
    outputprFile_ls.write(str(rp)+' '+str(xi_ls[i])+' '+str(ejn_ls[i])+'\n')
    
outputfsFile.close()
outputprFile_nat.close()
outputprFile_dp.close()
outputprFile_ham.close()
outputprFile_ls.close()

t2 = time.time()

print t2-t1

t100=time.time()
#%%
print 'Total time elapsed:', (t100-t0)/60.,'m'
timeFile.write('Nbins='+str(nbins)+'. Total time elapsed:'+str((t100-t0)/60.)+'m'+'\n')
timeFile.close()

#%%
import graf_ws
sample='RedGal'
graf_ws.plot(sample,rpf,xi_nat,xi_dp,xi_ls,xi_ham,ejn_nat,ejn_dp,ejn_ls,ejn_ham,nbins)