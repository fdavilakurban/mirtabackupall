c Agrega z a los puntos random

      parameter(ngal=766069,nran=966082)
      real zg(ngal) 
      character*24 dat(nran),da

c      open(10,file='../Catalogos-SDSS-DR4/Low-z/Lum/ColTracers/
c     1bluest.dat',status='old') 
c      open(20,file='testtar.ran',status='old') 
c      open(30,file='../Catalogos-SDSS-DR4/Low-z/Lum/ColTracers/
c     1bluest.ran',status='unknown') 

c      open(10,file='../../CorrelQSOs/QSDSS/qsoOptical.dat',status='old')
      open(10,file='../GalsDR6/redgals.dat',status='old')
      open(20,file='randomsB.ran',status='old') 
      open(30,file='../GalsDR6/redgals.ran',status='unknown')
c      open(30,file='../../CorrelQSOs/QSDSS/qsoOptical.ran',
c     1     status='unknown') 

      n=0
      do i=1,ngal
        read(10,11,end=100)zgal
        n=n+1
        zg(n)=zgal
100   end do      
c11    format(26x,f6.4)     
c11    format(37x,f7.5,27x) !formato para quasars    
11    format(28x,f9.7,96x) !formato para Gxs    
      k=0
      do j=1,nran
        read(20,22,end=200)da  
        k=k+1
        dat(k)=da
200   end do
22    format(a24)

      do i=1,n
        do j=1,k
          if(j.gt.3.0*(i-1))then
            if(j.le.3.0*i)write(30,33)dat(j),zg(i)
          end if
        end do
      end do
33    format(a24,2x,f7.5)
      end     
