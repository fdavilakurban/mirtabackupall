# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 22:49:55 2015

Asignar z a glxs random

@author: federico
"""
#%%

c = 300000.
H_0 = 70.

percent = 10
sample = 'gmenores'
ransample = 'random_MGS_DR10.fits'

#%%
import time
t1=time.time()

print 'Reading file: ', ransample
hdulist = fits.open(ransample,memmap=True)
rancat = Table(hdulist[1].data)
print '--> nr of rows : ', len(rancat)
#%%
#rancat.remove_column('id_pixel')

galf  = '{}.txt'.format(sample) # data sample
fr = fast_reader={'parallel': True, 'use_fast_converter': True}
print 'Reading file: ', galf
gals = ascii.read(galf,delimiter=' ',fast_reader=fr,fill_values=[('','nan','sigmaz'),('','nan','zprom')])
print '--> nr of rows : ', len(gals)

gals['sigmaz']*=H_0/c

print 'Creating random list'
rand = [rancat[i].as_void() for i in random.sample(xrange(len(rancat)), percent*len(gals))]
#rand = Table(rand)
print '--> nr of rows : ', len(rand)

#%%
rannew=np.zeros((len(rand),3))
k=0

for i in range(len(gals)):
    for j in range(percent):
        #if gals['sigmaz'][i] != 'nan': 
        #    z=random.gauss(gals['zprom'][i],gals['sigmaz'][i])
        #else: z=gals['zprom'][i]
        #print i
        z=gals['zprom'][i]
        a=rand[k][0],rand[k][1],z
        rannew[k]=a
        k+=1
#%%
ascii.write(rannew,'random_{}.txt'.format(sample),format='no_header')

t2=time.time()

print 'time:',t2-t1,'s'
#%%
import matplotlib.pyplot as plt

gals = ascii.read(galf,delimiter=' ',fast_reader=fr)
a = ascii.read('random_{}.txt'.format(sample),delimiter=' ',fast_reader=fr)#,fill_values=('nan'))

bins=10

#a=Table(rannew)

plt.close()

plt.hist(gals['zprom'], bins=bins, histtype='stepfilled', normed=True, color='b',label=sample)
plt.hist(a['col3'], bins=bins, histtype='stepfilled', normed=True, color='r', alpha=.5,label='random')

plt.title("Redshift Distribution")
plt.xlabel("z")
#plt.ylabel()
plt.legend()
plt.show()    


