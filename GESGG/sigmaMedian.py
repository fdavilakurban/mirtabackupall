# -*- coding: utf-8 -*-
"""
Created on Tue May 31 11:29:13 2016

@author: vvvteam
"""

#execfile('sigma5_paper.py')
import bootstrap as bs

sMedian_p = np.median(sigma_p)
x=np.array(sigma_p)
bs_p = bs.bootstrap(x,1000)

sMedian_t = np.median(sigma_t)
x=np.array(sigma_t)
bs_t = bs.bootstrap(x,1000)

sMedian_gm = np.median(sigma_gm)
x=np.array(sigma_gm)
bs_gm = bs.bootstrap(x,1000)

plt.figure()

plt.errorbar(1, sMedian_p, yerr=bs_p,fmt='o',capthick=2,label='Pairs')
plt.errorbar(2, sMedian_t, yerr=bs_t,fmt='o',capthick=2,label='Triplets')
plt.errorbar(3, sMedian_gm, yerr=bs_gm,fmt='o',capthick=2,label='Groups')

#plt.legend(fancybox=True,frameon=True,shadow=True,loc=4)

ticks = ['Pairs','Triplets','Groups']
plt.xticks([1,2,3],ticks,rotation=0)

plt.xlim([0.5,3.5])
plt.ylim([sMedian_p-.05,sMedian_gm+.05])
plt.ylabel('$<\Sigma{}>$'.format(str(sigma)))

plt.show()
#%%
plt.savefig('sigma{}median_paper.pdf'.format(sigma))
