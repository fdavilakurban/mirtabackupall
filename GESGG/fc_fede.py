# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 09:49:28 2015


Funcion de correlacion

Estimadores: nat, dp, ham, ls


@author: federico
"""
#%%
import numpy as np
import time
import datetime

#%%
timeFile = open('../ProgramasGeorgi/CorrelationFunction/fede/timertotal.txt','a')
timeFile.write('Inicio:'+datetime.datetime.now().strftime('%H:%M:%S')+'\n')

ts1 = time.time()


Om = 0.3
c = 300000.
H_0 = 70.
pr = np.pi/180.

nbdp = 10
nbdv = 10

rmax = np.log10(20.)
rmin = np.log10(0.15)

zmax = 60.
zmin = 0.0001

dbin = (rmax-rmin)/float(nbdp)
zbin = (zmax-zmin)/float(nbdv) 

fdd = np.zeros((nbdp,nbdv),'int') #Array bidimensional (ceros, enteros) 
frr = np.zeros((nbdp,nbdv),'int') #Array bidimensional (ceros, enteros) 

ddjn = np.zeros((10,nbdp,nbdv),'int') # Array tridimensional 10 x nbdp x nbdv ceros
rrjn = np.zeros((10,nbdp,nbdv),'int') # Array tridimensional 10 x nbdp x nbdv ceros

njndd = np.zeros((10),'int')  # Inicializo un contador para el numero de objetos
njnrr = np.zeros((10),'int')  # que tienen vecinos para cada jackknife

ndiv = 1 # Numero entero en el cual dividir la muestra de tracers. Solo para probar resultados rapidos. 

ts2 = time.time()

ts2-ts1

#%%
ts1 = time.time()

nmuestra = 500   # Si quiero limitar la muestra de targets y target-randoms, escribo un valor aqu{i

#----TRACERS------------------------------------------------------------
atc = []
dtc = []
ztc = []



readFile1 = open('../ProgramasGeorgi/CorrelationFunction/catalogos/DR4NS.dat','r')

line1 = readFile1.read().split('\n')

nd4 = 0
i = 0
for var in line1:

    i = i+1    
        
    if var == '': break
    
    sepList1 = var.split()

    if float(sepList1[2]) > .11 or float(sepList1[2]) < .06: continue
    if float(sepList1[2]) < 0.: 
        print "z negativo archivo 1 en linea:",i
        continue
    atc.append((float(sepList1[0])*pr))
    dtc.append((float(sepList1[1])*pr))
    ztc.append((float(sepList1[2])*c/H_0))
    
    nd4=nd4+1
    
#outputFile = open('../ProgramasGeorgi/CorrelationFunction/fede/adz.tracers','w')
#for i in range(nd4): outputFile.write(str(atc[i])+' '+str(dtc[i])+' '+str(ztc[i])+'\n')
#outputFile.close()

#RANDOM------------------------------------------------------------------------------------------------
######################################################################################################
arr = []
drr = []
zrr = []

readFile2 = open('../ProgramasGeorgi/CorrelationFunction/catalogos/ranDR4GalNS.dat','r')

line2 = readFile2.read().split('\n')

nr4 = 0

i = 0
for var in line2:
    
    i = i+1    
    
    if var == '': break
    
    sepList2 = var.split()

    if float(sepList2[2]) > .11 or float(sepList2[2]) < .06: continue
    if float(sepList2[2]) < 0.: 
        print "z negativo archivo 2 en linea:",i
        continue            
    arr.append(float(sepList2[0])*pr)
    drr.append(float(sepList2[1])*pr)
    zrr.append(float(sepList2[2])*c/H_0)
    
    nr4=nr4+1
    
#outputFile = open('../ProgramasGeorgi/CorrelationFunction/fede/adz.random','w')
#for i in range(nr4): outputFile.write(str(arr[i])+' '+str(drr[i])+' '+str(zrr[i])+'\n')
#outputFile.close()


#TARGET------------------------------------------------------------------------------------------------
######################################################################################################
att = []
dtt = []
ztt = []

readFile3 = open('../ProgramasGeorgi/CorrelationFunction/fc/redgalN.dat','r')

line3 = readFile3.read().split('\n')

nt = 0
i = 0
for var in line3:

    i = i+1                    
    
    if var == '': break
    
    sepList3 = var.split()
    if float(sepList3[2]) > .11 or float(sepList3[2]) < .06: continue
    if float(sepList3[2]) < 0.: 
        print "z negativo archivo 3 en linea:",i
        continue

    att.append((float(sepList3[0])*pr))
    dtt.append((float(sepList3[1])*pr))
    ztt.append((float(sepList3[2])*c/H_0))
    
    nt=nt+1
    
#outputFile = open('../ProgramasGeorgi/CorrelationFunction/fede/adz.targets','w')
#for i in range(nt): outputFile.write(str(att[i])+' '+str(dtt[i])+' '+str(ztt[i])+'\n')
#outputFile.close()


#TARGET-RANDOM------------------------------------------------------------------------------------------------
######################################################################################################
atr = []
dtr = []
ztr = []

readFile4 = open('../ProgramasGeorgi/CorrelationFunction/fc/redgalN.ran','r')

line4 = readFile4.read().split('\n')

nr = 0
i = 0
for var in line4:

    i = i+1                    
    if var == '': break
    
    sepList4 = var.split()
    if float(sepList4[2]) > .11 or float(sepList4[2]) < .06: continue
    if float(sepList4[2]) < 0.: 
        print "z negativo archivo 4 en linea:",i
        continue    
    atr.append(float(sepList4[0])*pr)
    dtr.append(float(sepList4[1])*pr)
    ztr.append(float(sepList4[2])*c/H_0)
    
    nr=nr+1
    
#outputFile = open('../ProgramasGeorgi/CorrelationFunction/fede/adz.targetrandoms','w')
#for i in range(nr): outputFile.write(str(atr[i])+' '+str(dtr[i])+' '+str(ztr[i])+'\n')
#outputFile.close()

ts2 = time.time()
ts2-ts1
print nd4,nr4,nt,nr

#%%
ts1 = time.time()

#======ParaJackknife====================================================

nextt = float(nt)/10. # Jackknife. (En el programa de Georgi la varible es 'next', que en Python es una función, asique la llamo 'nextt')

# =====================================================================
# ************* Calculo de data-data (target-tracer) ******************
# =====================================================================

ntt = 0

for i in range(nt-1): # Hace un lazo para todos los targets
    print i,'/',nt-1
    #if i==1000 or i==10000 or i==40000: print i
    z1 = ztt[i]+60. # Limite en distancia radial a los targets
    z2 = ztt[i]-60.
    zc = ztt[i]*H_0/c 
    
    ntv = 0
    njackex = int(float(i+1)/nextt) # Jackknife
    
    for j in range((nd4-1)/ndiv): # Hace un lazo para los tracers. 'nd4-1' pq len(ztc)=nd4, pero ztc[0,1,...,nd4-1]
         
        if ztc[j] >= z1: continue # Los que no estan dentro de los limites, los salta
        if ztc[j] <= z2: continue      
            
        zv = ztc[j]*H_0/c  

# ------  Calculo de la distancia proyectada ---------------------------

        zz = (zc+zv)/2.
        rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5))) ) / (H_0*(Om**2)*(1.+zz)**2)

        da = abs(atc[j]-att[i])
    
        if da > np.pi: da = 2.*np.pi-da
        if da == 0.: continue
            
        dd = (np.sin(dtc[j])*np.sin(dtt[i])) + (np.cos(dtc[j])*np.cos(dtt[i]))*np.cos(da)
                
        if abs(dd) > 1.:
            if dd < -1.: dd=-1.
            if dd > 1.: dd=1.
        dd = np.arccos(dd)    
        
        rp = rz*dd
        rpl = np.log10(rp)

        if rpl >= rmax or rpl <= rmin: continue
            
# ------  Calculo de la distancia radial -------------------------------

        difd = abs(ztt[i]-ztc[j])
        didl = difd
                
        if didl >= zmax or didl <= zmin: continue
        
# ------  Bineado en rp y deltaV ---------------------------------------         

        ind = int((rpl-rmin)/dbin)  # ind y inz toman valores
        inz = int((didl-zmin)/zbin) # entre 0 y 9
                             
        fdd[ind,inz] = fdd[ind,inz]+1 
        ntv = ntv+1  
            
# ----------------------------------------------------------------
# ------Errores Jackknife -----------------------------------------   
        
        
        for k in range(10): #jackknife k=0,...,9
            if k != njackex: 
                ddjn[k,ind,inz] = ddjn[k,ind,inz]+1
                njndd[k] = njndd[k]+1       
                
# ----------------------------------------------------------------
    
    if ntv > 0: ntt = ntt+1
        
#print 'i final:',i
#print ntt,' galaxias tienen vecinos'                    

ts2 = time.time()
ts2-ts1

#%%
ts1 = time.time()

# ======Para Jackknife===============================================================

nexr = float(nr)/10. # Jackknife.

# =====================================================================
# ************* Calculo de randomtarget-random ******************
# =====================================================================

nrr = 0

for i in range(nr-1): # Hace un lazo para todos los target random
    print i,'/',nr-1
    #if i==10000 or i==100000 or i==400000: print i
    z1 = ztr[i]+60. # Limite en distancia radial a los tracers
    z2 = ztr[i]-60.
    zc = ztr[i]*H_0/c 
    
    nrv = 0
    njackex = int(float(i+1)/nexr) # Jackknife
    
    for j in range((nr4-1)/ndiv):  # Hace un lazo para los tracers. 'nd4-1' pq len(ztc)=nd4, pero ztc[0,1,...,nd4-1]
         
        if zrr[j] >= z1: continue # Los que no estan dentro de los limites, los salta
        if zrr[j] <= z2: continue        
           
        zv = zrr[j]*H_0/c  

# ------  Calculo de la distancia proyectada ---------------------------

        zz = (zc+zv)/2.
        rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5))) ) / (H_0*(Om**2)*(1.+zz)**2)

        da = abs(arr[j]-atr[i])
    
        if da > np.pi: da = 2.*np.pi-da
        if da == 0.: continue
            
        dd = (np.sin(drr[j])*np.sin(dtr[i])) + (np.cos(drr[j])*np.cos(dtr[i]))*np.cos(da)
                
        if abs(dd) > 1.:
            if dd < -1.: dd=-1.
            if dd > 1.: dd=1.
        dd = np.arccos(dd)    
        
        rp = rz*dd
        rpl = np.log10(rp)

        if rpl >= rmax or rpl <= rmin: continue

# ------  Calculo de la distancia radial -------------------------------

        difr = abs(ztr[i]-zrr[j])
        didl = difr
                
        if didl >= zmax or didl <= zmin: continue
    
# ------  Bineado en rp y deltaV ---------------------------------------         

        ind = int((rpl-rmin)/dbin) 
        inz = int((didl-zmin)/zbin) 
                             
        frr[ind,inz] = frr[ind,inz]+1 
        nrv = nrv+1  
            
# ----------------------------------------------------------------
# ------Errores Jackknife -----------------------------------------   
        
        for k in range(10): # k=0,...,9
            if k != njackex: 
                rrjn[k,ind,inz] = rrjn[k,ind,inz]+1
                njnrr[k] = njnrr[k]+1      
                
# ----------------------------------------------------------------
    
    if nrv > 0: nrr = nrr+1
        
#print 'i final:',i
#print nrr,' galaxias tienen vecinos'                    

ts2 = time.time()
ts2-ts1

#%%
ts1 = time.time()

fdr = np.zeros((nbdp,nbdv),'int') #Array bidimensional (ceros, enteros) 

drjn = np.zeros((10,nbdp,nbdv),'int') # Array tridimensional 10 x nbdp x nbdv ceros

njndr = np.zeros((10),'int')  # que tienen vecinos para cada jackknife

# =====================================================================
# ************* Calculo de target-random ******************
# =====================================================================

ntr = 0

for i in range(nt-1): # Hace un lazo para todos los targets
    print i,'/',nt-1
    #if i==10000 or i==100000 or i==400000: print i
    z1 = ztt[i]+60. # Limite en distancia radial a los targets
    z2 = ztt[i]-60.
    zc = ztt[i]*H_0/c 
    
    ntrv = 0
    njackex = int(float(i+1)/nextt) # Jackknife
    
    for j in range((nr4-1)/ndiv):  # Hace un lazo para los tracers random '
                                 # 'nd4-1' pq len(ztc)=nd4, pero ztc[0,1,...,nd4-1]
         
        if zrr[j] >= z1: continue # Los que no estan dentro de los limites, los salta
        if zrr[j] <= z2: continue        
           
        zv = zrr[j]*H_0/c  

# ------  Calculo de la distancia proyectada ---------------------------

        zz = (zc+zv)/2.
        rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5))) ) / (H_0*(Om**2)*(1.+zz)**2)

        da = abs(arr[j]-att[i])
    
        if da > np.pi: da = 2.*np.pi-da
        if da == 0.: continue
            
        dd = (np.sin(drr[j])*np.sin(dtt[i])) + (np.cos(drr[j])*np.cos(dtt[i]))*np.cos(da)
                
        if abs(dd) > 1.:
            if dd < -1.: dd=-1.
            if dd > 1.: dd=1.
        dd = np.arccos(dd)    
        
        rp = rz*dd
        rpl = np.log10(rp)

        if rpl >= rmax or rpl <= rmin: continue

# ------  Calculo de la distancia radial -------------------------------

        difr = abs(ztt[i]-zrr[j])
        didl = difr
                
        if didl >= zmax or didl <= zmin: continue
    
# ------  Bineado en rp y deltaV ---------------------------------------         

        ind = int((rpl-rmin)/dbin) 
        inz = int((didl-zmin)/zbin) 
                             
        fdr[ind,inz] = fdr[ind,inz]+1 
        ntrv = ntrv+1  
            
# ----------------------------------------------------------------
# ------Errores Jackknife -----------------------------------------   
        
        for k in range(10): #jackknife k=0,...,9
            if k != njackex: 
                drjn[k,ind,inz] = drjn[k,ind,inz]+1
                njndr[k] = njndr[k]+1      
                
# ----------------------------------------------------------------
    
    if ntrv > 0: ntr = ntr+1
        
ts2 = time.time()
ts2-ts1

#%%
ts1 = time.time()

xjn_nat = np.zeros((10,nbdp),'float')
xjn_dp = np.zeros((10,nbdp),'float')
xjn_ham = np.zeros((10,nbdp),'float')
xjn_ls = np.zeros((10,nbdp),'float')

xm_nat = np.zeros((nbdp),'float')
xm_dp = np.zeros((nbdp),'float')
xm_ham = np.zeros((nbdp),'float')
xm_ls = np.zeros((nbdp),'float')

ejn_nat = np.zeros((nbdp),'float')
ejn_dp = np.zeros((nbdp),'float')
ejn_ham = np.zeros((nbdp),'float')
ejn_ls = np.zeros((nbdp),'float')

outputjnFile_nat = open('../ProgramasGeorgi/CorrelationFunction/fede/outputjn.nat.txt','w')
outputjnFile_dp = open('../ProgramasGeorgi/CorrelationFunction/fede/outputjn.dp.txt','w')
outputjnFile_ham = open('../ProgramasGeorgi/CorrelationFunction/fede/outputjn.ham.txt','w')
outputjnFile_ls = open('../ProgramasGeorgi/CorrelationFunction/fede/outputjn.ls.txt','w')

# ======Errores Jackknife===============================================================

for k in range(10):
    djn1 = float(njnrr[k])*float(nr4)
    djn2 = float(njndd[k])*float(nd4)
    rjnorm = djn1/djn2    
    
    for i in range(nbdp):
        
        for j in range(nbdv):
            if ddjn[k,i,j] == 0 or rrjn[k,i,j] == 0 or drjn[k,i,j] == 0: continue
            
            wwjn_nat = float(ddjn[k,i,j])/float(rrjn[k,i,j])
            wwjn_dp  = float(ddjn[k,i,j])/float(drjn[k,i,j])
            wwjn_ham = float(ddjn[k,i,j])*float(rrjn[k,i,j])/(float(drjn[k,i,j])**2.)
            wwjn_ls  = float(drjn[k,i,j])/float(rrjn[k,i,j])
            
            xjn_nat[k,i] = xjn_nat[k,i]+wwjn_nat*(rjnorm)**2.-1.
            xjn_dp[k,i]  = xjn_dp[k,i]+wwjn_dp*rjnorm-1.
            xjn_ham[k,i] = xjn_ham[k,i]+wwjn_ham-1.
            xjn_ls[k,i]  = xjn_ls[k,i]+wwjn_nat*(rjnorm)**2-wwjn_ls*rjnorm*2.+1.
            
        xjn_nat[k,i] = 2.*xjn_nat[k,i]*zbin 
        xjn_dp[k,i]  = 2.*xjn_dp[k,i]*zbin            
        xjn_ham[k,i] = 2.*xjn_ham[k,i]*zbin            
        xjn_ls[k,i]  = 2.*xjn_ls[k,i]*zbin            
        

for i in range(nbdp):
    
    for k in range(10):
        xm_nat[i] = xm_nat[i]+xjn_nat[k,i] 
        xm_dp[i]  = xm_dp[i]+xjn_dp[k,i] 
        xm_ham[i] = xm_ham[i]+xjn_ham[k,i] 
        xm_ls[i]  = xm_ls[i]+xjn_ls[k,i] 

    xm_nat[i] = xm_nat[i]/10.
    xm_dp[i]  = xm_dp[i]/10.
    xm_ham[i] = xm_ham[i]/10.
    xm_ls[i]  = xm_ls[i]/10.

for i in range(nbdp):
    rp = rmin+dbin/2.*(2.*float(i+1)-1.)
    rp = 10.**rp
    
    for k in range(10):
        ejn_nat[i] = ejn_nat[i]+((xjn_nat[k,i]-xm_nat[i])**2)
        ejn_dp[i]  = ejn_dp[i]+((xjn_dp[k,i]-xm_dp[i])**2)
        ejn_ham[i] = ejn_ham[i]+((xjn_ham[k,i]-xm_ham[i])**2)
        ejn_ls[i]  = ejn_ls[i]+((xjn_ls[k,i]-xm_ls[i])**2)
        
        outputjnFile_nat.write(str(k)+' '+str(rp)+' '+str(xm_nat[i])+' '+str(xjn_nat[k,i])+'\n')
        outputjnFile_dp.write(str(k)+' '+str(rp)+' '+str(xm_dp[i])+' '+str(xjn_dp[k,i])+'\n')
        outputjnFile_ham.write(str(k)+' '+str(rp)+' '+str(xm_ham[i])+' '+str(xjn_ham[k,i])+'\n')
        outputjnFile_ls.write(str(k)+' '+str(rp)+' '+str(xm_ls[i])+' '+str(xjn_ls[k,i])+'\n')
    
    ejn_nat[i]= np.sqrt(ejn_nat[i]/10.)
    ejn_dp[i]= np.sqrt(ejn_dp[i]/10.)
    ejn_ham[i]= np.sqrt(ejn_ham[i]/10.)
    ejn_ls[i]= np.sqrt(ejn_ls[i]/10.)

outputjnFile_nat.close()
outputjnFile_dp.close()
outputjnFile_ham.close()
outputjnFile_ls.close()


ts2 = time.time()
ts2-ts1

#%%
ts1 = time.time()

# ======================================================================
# -------------- Calculo la Funcion de Correlacion ---------------------
# ======================================================================

xi_nat = np.zeros((10),'float')
xi_dp  = np.zeros((10),'float')
xi_ham = np.zeros((10),'float')
xi_ls  = np.zeros((10),'float')

w_nat = np.zeros((nbdp,nbdv),'float')
w_dp  = np.zeros((nbdp,nbdv),'float')
w_ham = np.zeros((nbdp,nbdv),'float')
w_ls  = np.zeros((nbdp,nbdv),'float')

dn1 = float(nrr)*float(nr4)
dn2 = float(ntt)*float(nd4)
rnorm = dn1/dn2

outputfsFile = open('../ProgramasGeorgi/CorrelationFunction/fede/output.fs.txt','w')

outputprFile_nat = open('../ProgramasGeorgi/CorrelationFunction/fede/output.pr.nat.txt','w')
outputprFile_dp  = open('../ProgramasGeorgi/CorrelationFunction/fede/output.pr.dp.txt','w')
outputprFile_ham = open('../ProgramasGeorgi/CorrelationFunction/fede/output.pr.ham.txt','w')
outputprFile_ls  = open('../ProgramasGeorgi/CorrelationFunction/fede/output.pr.ls.txt','w')

for i in range(nbdp):
    rp = rmin+dbin/2.*(2.*float(i+1)-1.) # Codigo en Fortran dice float(i)
    rp = 10.**rp
            
    for j in range(nbdv):
            
        if fdd[i,j] == 0. or frr[i,j] == 0. or fdr[i,j] == 0.: continue
        ww_nat = float(fdd[i,j])/float(frr[i,j])
        ww_dp  = float(fdd[i,j])/float(fdr[i,j])
        ww_ham = float(fdd[i,j])*float(frr[i,j])/(float(fdr[i,j])**2)
        ww_ls  = float(fdr[i,j])/float(frr[i,j])
 
        xi_nat[i] = xi_nat[i]+ww_nat*rnorm**2-1.
        xi_dp[i]  = xi_dp[i]+ww_dp*rnorm-1.
        xi_ham[i] = xi_ham[i]+ww_ham-1.
        xi_ls[i]  = xi_ls[i]+ww_nat*rnorm**2-2.*rnorm*ww_ls+1.
        
        outputfsFile.write(str(i)+' '+str(j)+' '+str(fdd[i,j])+' '+str(frr[i,j])+' '+str(fdr[i,j])+'\n')
        
    xi_nat[i] = 2.*xi_nat[i]*zbin
    xi_dp[i]  = 2.*xi_dp[i]*zbin
    xi_ham[i] = 2.*xi_ham[i]*zbin
    xi_ls[i]  = 2.*xi_ls[i]*zbin
    
    outputprFile_nat.write(str(rp)+' '+str(xi_nat[i])+' '+str(ejn_nat[i])+'\n')
    outputprFile_dp.write(str(rp)+' '+str(xi_dp[i])+' '+str(ejn_dp[i])+'\n')
    outputprFile_ham.write(str(rp)+' '+str(xi_ham[i])+' '+str(ejn_ham[i])+'\n')
    outputprFile_ls.write(str(rp)+' '+str(xi_ls[i])+' '+str(ejn_ls[i])+'\n')
    
outputfsFile.close()
outputprFile_nat.close()
outputprFile_dp.close()
outputprFile_ham.close()
outputprFile_ls.close()

ts2 = time.time()
timeFile.write('Fin:'+datetime.datetime.now().strftime('%H:%M:%S')+'\n')
timeFile.close()
ts2-ts1

