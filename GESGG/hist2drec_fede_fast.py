# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 19:47:04 2015

NO ESTA TERMINADO

@author: federico
"""

import numpy as np
from astropy.table import Table

def hist2d_fede(data, bins, plot=False, plttitle=''):
    
    x=data['ra']
    y=data['dec']
    z=data['z']
    id_gr=data['groupID']

    
#    x=[]
#    y=[]
#    z=[]
    nbins=bins
    
    
#    for i in gals: 
#        x.append(i[0])
#        y.append(i[1])
#        z.append(i[2])
    
    if plot == True:
        import matplotlib.pyplot as plt
        plt.figure()        
        plt.hist2d(x,y,bins=nbins)
        plt.colorbar() 
        plt.title(plttitle)
        plt.show()        
#%%        
    H, xedges, yedges = np.histogram2d(x,y,bins=nbins)

    H=H.T # Transpose
    
    cgal=np.zeros((len(x),4))
    
    for i in range(len(x)): cgal[i]=x[i],y[i],z[i],float(id_gr[i])
    
    cgal=Table(cgal,names=['ra','dec','z','groupID'])
#%%    
    #coord=np.zeros((nbins[1],nbins[0],H.max(),4)) # Guarda ra y dec p/las gal en c/bin    
    coord=Table(names=['ra','dec','z','groupID'],dtype=[float,float,float,int])
    for i in range(nbins[1]):
        print i,'/',nbins[1]-1
        for j in range(nbins[0]):
            rows = np.where((cgal['ra']>=xedges[j]) & (cgal['ra']<xedges[j+1]) & (cgal['dec']>=yedges[i]) & (cgal['dec']<yedges[i+1]))            
            for k in range(len(cgal[rows])):
                coord.insert_row([i,j,cgal[rows][k]])
"""
TERMINAR
"""
#%%    
#    cgal=cgal[cgal[:,0].argsort()]  # Sort by the first column
#    
#    
#    coord=np.zeros((nbins[1],nbins[0],H.max(),4)) # Guarda ra y dec p/las gal en c/bin
#    
#    hsum=H.sum(0) # Guarda el num de obj en cada columna 
#    
#    for i in range(nbins[1]):                  # fila i del hist
#        m=-1
#        print i,'/',nbins[1]-1
#        for j in range(nbins[0]):              # columna j del hist
#            k=0
#            for l in range(int(hsum[j])):      # num de gal en la col. j
#                m+=1
#                if yedges[i] <= cgal[m,1] < yedges[i+1]:
#                    coord[i,j,k]=cgal[m]
#                    k+=1
#                if k==H[i,j]+1: continue # pq no hay mas objetos en el bin i,j

    return coord,H






