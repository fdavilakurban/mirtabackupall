# -*- coding: utf-8 -*-
"""
Created on Sun Sep 13 23:00:12 2015

Este programa podria usarse para limpiar catalogos.
Luego, en el programa que use los catalogos, que los lea de los archivos
que produce este programa.

@author: federico
"""

#%%
import time
import numpy as np


H_0 = 70.
c = 300000.
pr = np.pi/180.
delta_z = 60.*H_0/c       # Para limitar en distancia radial 
zmin, zmax = .06, .11     # Rango de z de los targets
z1, z2 = zmin-delta_z, zmax+delta_z

#%%
ts1 = time.time()


#======TRACERS==================================
atc = []
dtc = []
ztc = []
#mtc = []

readFile1 = open('../ProgramasGeorgi/CorrelationFunction/catalogos/DR4NS.dat','r')

line1 = readFile1.read().split('\n')

nd4 = 0
i = 0
for var in line1:

    i = i+1    
        
    if var == '': break
    
    sepList1 = var.split()

    if float(sepList1[2]) > z2 or float(sepList1[2]) < z1: continue
    if float(sepList1[2]) <= 0.: 
        print "z negativo (o nulo) archivo 1 en linea:",i
        continue
    #if float(sepList1[5]) > -20.5: continue    
    atc.append((float(sepList1[0])*pr))
    dtc.append((float(sepList1[1])*pr))
    ztc.append((float(sepList1[2])*c/H_0))
    #mtc.append(float(sepList1[5]))
    nd4=nd4+1

  
outputFile = open('../ProgramasGeorgi/CorrelationFunction/fede/adz.tracers','w')
for i in range(nd4): outputFile.write(str(atc[i])+' '+str(dtc[i])+' '+str(ztc[i])+'\n')#str(mtc[i])+'\n')
outputFile.close()

#%%
#===RANDOM=======================================

arr = []
drr = []
zrr = []

readFile2 = open('../ProgramasGeorgi/CorrelationFunction/catalogos/ranDR4GalNS.dat','r')

line2 = readFile2.read().split('\n')

nr4 = 0

i = 0
for var in line2:
    
    i = i+1    
    
    if var == '': break
    
    sepList2 = var.split()

    if float(sepList2[2]) > z2 or float(sepList2[2]) < z1: continue
    if float(sepList2[2]) <= 0.: 
        print "z negativo (o nulo) archivo 2 en linea:",i
        continue            
    arr.append((float(sepList2[0])*pr))
    drr.append((float(sepList2[1])*pr))
    zrr.append((float(sepList2[2])*c/H_0))
    
    nr4=nr4+1
    
outputFile = open('../ProgramasGeorgi/CorrelationFunction/fede/adz.random','w')
for i in range(nr4): outputFile.write(str(arr[i])+' '+str(drr[i])+' '+str(zrr[i])+'\n')
outputFile.close()

#%%
#TARGET------------------------------------------------------------------------------------------------
######################################################################################################
att = []
dtt = []
ztt = []

readFile3 = open('../ProgramasGeorgi/CorrelationFunction/fc/redgalN.dat','r')

line3 = readFile3.read().split('\n')

nt = 0
for var in line3:

    if var == '': break
    
    sepList3 = var.split()
    if float(sepList3[2]) > z2 or float(sepList3[2]) < z1: continue
    if float(sepList3[2])<= 0.: 
        print "z negativo (o nulo) archivo 3 en linea:",i
        continue

    att.append((float(sepList3[0])*pr))
    dtt.append((float(sepList3[1])*pr))
    ztt.append((float(sepList3[2])*c/H_0))
    
    nt=nt+1
    
outputFile = open('../ProgramasGeorgi/CorrelationFunction/fede/adz.targets_red','w')
for i in range(nt): outputFile.write(str(att[i])+' '+str(dtt[i])+' '+str(ztt[i])+'\n')
outputFile.close()
#%%

#TARGET-RANDOM------------------------------------------------------------------------------------------------
######################################################################################################
atr = []
dtr = []
ztr = []

readFile4 = open('../ProgramasGeorgi/CorrelationFunction/fc/redgalN.ran','r')

line4 = readFile4.read().split('\n')

nr = 0
i = 0
for var in line4:

    i = i+1                    
    if var == '': break
    
    sepList4 = var.split()
    if float(sepList4[2]) > z2 or float(sepList4[2]) < z1: continue
    if float(sepList4[2]) <= 0.: 
        print "z negativo (o nulo) archivo 4 en linea:",i
        continue    
    atr.append((float(sepList4[0])*pr))
    dtr.append((float(sepList4[1])*pr))
    ztr.append((float(sepList4[2])*c/H_0))
    
    nr=nr+1
    
outputFile = open('../ProgramasGeorgi/CorrelationFunction/fede/adz.targetrandoms_red','w')
for i in range(nr): outputFile.write(str(atr[i])+' '+str(dtr[i])+' '+str(ztr[i])+'\n')
outputFile.close()
#%%
ts2 = time.time()
print ts2-ts1
print nd4,nr4,nt,nr