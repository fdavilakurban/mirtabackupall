# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 19:47:04 2015

@author: federico
"""

import numpy as np


def hist2d_fede(gals, bins, plot=False, plttitle=''):

    x=[]
    y=[]
    z=[]
    nbins=bins
    
    for i in gals: 
        x.append(i[0])
        y.append(i[1])
        z.append(i[2])
    
    if plot == True:
        import matplotlib.pyplot as plt
        plt.figure()        
        plt.hist2d(x,y,bins=nbins)
        plt.colorbar() 
        plt.title(plttitle)
        plt.show()        
        
    H, xedges, yedges = np.histogram2d(x,y,bins=nbins)
    
    H=H.T # Transpose
    
    cgal=np.zeros((len(x),3))
    
    for i in range(len(x)): cgal[i]=x[i],y[i],z[i]
    
    cgal=cgal[cgal[:,0].argsort()]  # Sort by the first column
    
    
    coord=np.zeros((nbins,nbins,H.max(),3)) # Guarda ra y dec p/las gal en c/bin
    
    hsum=H.sum(0)
    
    for i in range(nbins):                          # fila i del hist
        m=-1
        for j in range(nbins):                      # columna j del hist
            k=0
            for l in range(int(hsum[j])):           # num de gal en la col. j
                m=m+1
                if yedges[i] <= cgal[m,1] < yedges[i+1]:
                    coord[i,j,k]=cgal[m]
                    k=k+1
                if k==H[j,i]: continue # pq no hay mas objetos en el bin i,j

    return coord,H






