# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 19:02:07 2015

PARA CALCULAR NUEVOS DATOS, COMENTARIAR ABAJO LA LECTURA DE DATOS VIEJOS

@author: federico
"""

#%%
import numpy as np
import time
#import datetime

sample='paresc'
bins=10
#%%
timeFile = open('timertotal_hist2d.txt','a')

t0 = time.time()


Om = 0.3
c = 300000.
H_0 = 100.
pr = np.pi/180.

nbdp = bins
nbdv = bins

rpf = np.zeros((nbdp),'float')

rpmax = 21.
rmax = np.log10(rpmax)
rmin = np.log10(0.23)

zmax = 42.
zmin = 0.0001

dbin = (rmax-rmin)/float(nbdp)
zbin = (zmax-zmin)/float(nbdv) 

fdd = np.zeros((nbdp,nbdv),'int') #Array bidimensional (ceros, enteros) 
frr = np.zeros((nbdp,nbdv),'int') 
fdr = np.zeros((nbdp,nbdv),'int') 

drjn = np.zeros((10,nbdp,nbdv),'int') # Array tridimensional 10 x nbdp x nbdv ceros
ddjn = np.zeros((10,nbdp,nbdv),'int') 
rrjn = np.zeros((10,nbdp,nbdv),'int') 

njndd = np.zeros((10),'int')  # Inicializo un contador para el numero de objetos
njnrr = np.zeros((10),'int')  
njndr = np.zeros((10),'int')  



#%%


t1 = time.time()

from astropy.io import ascii
from astropy.io import fits
from astropy.table import Table
from astropy.table import Column

galf  = '{}.txt'.format(sample) # data sample
rcalf  = 'random_MGS_DR10_maskedIDgr.fits.txt'  # random cross sample
calf  = 'MGS_DR10_maskedIDgr.fits' # cross sample
rgalf  = 'random_{0}.txt'.format(sample) # random target sample

# READ INPUT DATA (fast!) into astropy tables
cols = ['ra','dec','z']
fr = fast_reader={'parallel': True, 'use_fast_converter': True}
print 'Reading file: ', galf
gals = ascii.read(galf,delimiter=' ',fast_reader=fr)#,names=cols)
print '--> nr of rows : ', len(gals)
print 'Reading file: ', rcalf
rcals = ascii.read(rcalf,delimiter=' ',fast_reader=fr)#,names=cols)
col=Column(name='groupID',length=len(rcals))
rcals.add_column(col) 
print '--> nr of rows : ', len(rcals)
print 'Reading file: ', calf
hdulist = fits.open(calf,memmap=True)
cals = Table(hdulist[1].data)

# Restriccion en Luminosidad
x=np.where(cals['Mr']>=-19.) 
cals.remove_rows(x)

# Restriccion en z, 60Mpc por debajo y por encima del zmin=0.5 y zmax=.15
z1=.05-zmax*H_0/c
x=np.where(cals['z']<=z1)
cals.remove_rows(x)

z2=.15+zmax*H_0/c
x=np.where(cals['z']>=z2)
cals.remove_rows(x)

cals.rename_column('ra_1','ra')
cals.rename_column('dec_1','dec')
print '--> nr of rows : ', len(cals)
print 'Reading file: ', rgalf
rgals = ascii.read(rgalf,delimiter=' ',fast_reader=fr)#,names=cols)
print '--> nr of rows : ', len(rgals)

t2 = time.time()

print 'Time spent reading files: ',t2-t1,'s'

nt=len(gals)
nr=len(rgals)
nr4=len(rcals)
nd4=len(cals)
#%%
gals['Xcent'] *= np.pi/180. # a radianes
gals['Ycent'] *= np.pi/180.
gals['zprom'] *= c/H_0      # a 

rgals['ra'] *= np.pi/180. # a radianes
rgals['dec'] *= np.pi/180.
rgals['z'] *= c/H_0      # a Mpc

cals['ra'] *= np.pi/180. # a radianes
cals['dec']*= np.pi/180.
cals['z']  *= c/H_0      # a Mpc

rcals['ra'] *= np.pi/180. # a radianes
rcals['dec']*= np.pi/180.
rcals['z']  *= c/H_0      # a Mpc

gals.rename_column('Xcent','ra')
gals.rename_column('Ycent','dec')
gals.rename_column('zprom','z')
gals.rename_column('name','groupID')

#%%
t1=time.time()
a1=sorted(gals['ra'])
d1=sorted(gals['dec'])
z1=sorted(gals['z'])

zz = z1[0]*H_0/c # z min de la muestra
rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5)))) / (H_0*(Om**2)*(1.+zz)**2)

t2=time.time()

nbins=int((a1[-1]-a1[0])/(rpmax/rz)),int((d1[-1]-d1[0])/(rpmax/rz))

print 'Time spent calculating bins:',t2-t1,'s'

print 'Nbins=',nbins
#%%
print 'Creating the coordenates matrices...'
t1=time.time()
import hist2drec_fede

c_data,H_d=hist2drec_fede.hist2d_fede(gals,nbins)#,plot=True,plttitle='Data')
print 'Data Matrix created'
c_cross,H_c=hist2drec_fede.hist2d_fede(cals,nbins)#,plot=True,plttitle='Cross') 
print 'Cross Matrix created'
c_rdata,H_rd=hist2drec_fede.hist2d_fede(rgals,nbins)#,plot=True,plttitle='Random (Data)')
print 'Random-Data Matrix created'
c_rcross,H_rc=hist2drec_fede.hist2d_fede(rcals,nbins)#,plot=True,plttitle='Random (Cross)')
print 'Random-Cross Matrix created'
t2=time.time()
print 'Time spent creating the matrices: ',(t2-t1)/60.,'m'

#%%

# =====================================================================
# ************* Calculo de data-cross (target-tracer) ******************
# =====================================================================

t1 = time.time()

nextt = float(nt)/10. # Jackknife. 

ntt = 0
iflag=0
jflag=0
cont=0.

for i in range(nbins[1]):
    
    print 'DD',i,'/',nbins[1]-1
    for j in range(nbins[0]):
        
        if c_data[i,j,0,0] == 0.: continue # Revisar: no parece afectar
        for k in range(int(H_d[i,j])):
        
            z1 = c_data[i,j,k,2]+60. 
            z2 = c_data[i,j,k,2]-60.
            zc = c_data[i,j,k,2]*H_0/c 
            if  zc == 0.: 
                print 'zc=0;',i,j,k,'id:',c_data[i,j,k,3]                
                continue 
            
            cont+=1.
            ntv = 0            
            njackex = int(cont/nextt) # Jackknife

            for i1 in range(i-1,i+2):

                if i1==nbins[1]: 
                    i1 = 0
                    iflag = 1
                
                for j1 in range(j-1,j+2):
                    
                    if j1==nbins[0]: 
                        j1 = 0
                        jflag = 1
                        
                    for k1 in range(int(H_c[i1,j1])):
                        #print i1,j1,k1
                        if c_cross[i1,j1,k1,2] >= z1 or c_cross[i1,j1,k1,2] <= z2: continue
                        if c_data[i,j,k,3] == c_cross[i1,j1,k1,3]:
                            #print i,j,k,'/',i1,j1,k1,':','Pertenecientes al mismo grupo:',c_data[i,j,k,3],c_cross[i1,j1,k1,3]
                            continue
                        zv = c_cross[i1,j1,k1,2]*H_0/c              
        
#-----------------------Calculo de la Dist Proy------------------------
                        zz = (zc+zv)/2.
                        rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5)))) / (H_0*(Om**2)*(1.+zz)**2)
                
                        da = abs(c_cross[i1,j1,k1,0]-c_data[i,j,k,0])
                        if da == 0.: continue
                    
                        if da > np.pi: da = 2.*np.pi-da
                            
                        dd = (np.sin(c_cross[i1,j1,k1,1])*np.sin(c_data[i,j,k,1])) + (np.cos(c_cross[i1,j1,k1,1])*np.cos(c_data[i,j,k,1]))*np.cos(da)
                        if dd==0.: print 'dd=0'        
                                
                        if abs(dd) > 1.:                    
                                if dd < -1.: dd=-1.
                                if dd > 1.: dd=1.
                        dd = np.arccos(dd)    
                        
                        #rp = rz*dd
                        rpl = np.log10(rz*dd)
                
                        if rpl >= rmax or rpl <= rmin: continue                    
#---------------------------------------------------------------------
# --------------------  Calculo de la Dist Radial --------------------
        
                        didl = abs(c_data[i,j,k,2]-c_cross[i1,j1,k1,2])
                                
                        if didl >= zmax or didl <= zmin: continue
#---------------------------------------------------------------------
                        
# --------------------  Bineado en rp y deltaV -----------------------         
        
                        ind = int((rpl-rmin)/dbin)  # ind y inz toman valores
                        inz = int((didl-zmin)/zbin) # entre 0 y 9
                                             
                        fdd[ind,inz] += 1 
                        ntv += 1
#---------------------------------------------------------------------
#----------------------- Errores Jackknife----------------------------
                        
                        for l in range(10): #jackknife l=0,...,9
                            if l != njackex: 
                                ddjn[l,ind,inz] += 1
                                njndd[l] += 1                          
#---------------------------------------------------------------------
                      
                    if jflag == 1: 
                        j1=nbins[0]
                        jflag=0
                if iflag == 1: 
                    i1=nbins[1]
                    iflag=0                    
            if ntv > 0: ntt += 1


t2 = time.time()

print 'Data-cross time elapsed:',(t2-t1)/60.,'m'

#%%

# =====================================================================
# ************* Calculo de Randata-Rancross ******************
# =====================================================================

t1 = time.time()

nexr = float(nr)/10. # Jackknife

nrr = 0
iflag=0
jflag=0
cont=0.

for i in range(nbins[1]):
    
    print 'RR',i,'/',nbins[1]-1
    for j in range(nbins[0]):
        

        if c_rdata[i,j,0,0] == 0.: continue # Revisar: no parece afectar
        for k in range(int(H_rd[i,j])):
        
            z1 = c_rdata[i,j,k,2]+60. 
            z2 = c_rdata[i,j,k,2]-60.
            zc = c_rdata[i,j,k,2]*H_0/c 
            if  zc == 0.: 
                print 'zc=0;',i,j,k,'id:',c_rdata[i,j,k,3]                
                continue            
            nrv = 0
            cont+=1.
            njackex = int(cont/nexr) # Jackknife
                        
            for i1 in range(i-1,i+2):

                if i1==nbins[1]: 
                    i1 = 0
                    iflag = 1
                
                for j1 in range(j-1,j+2):
                    
                    if j1==nbins[0]: 
                        j1 = 0
                        jflag = 1
                        
                    for k1 in range(int(H_rc[i1,j1])):
                        if c_rcross[i1,j1,k1,2] >= z1 or c_rcross[i1,j1,k1,2] <= z2: continue

                        zv = c_rcross[i1,j1,k1,2]*H_0/c              
        
#-----------------------Calculo de la Dist Proy------------------------
                        zz = (zc+zv)/2.
                        rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5)))) / (H_0*(Om**2)*(1.+zz)**2)
                
                        da = abs(c_rcross[i1,j1,k1,0]-c_rdata[i,j,k,0])
                        if da == 0.: continue

                        if da > np.pi: da = 2.*np.pi-da
                            
                        dd = (np.sin(c_rcross[i1,j1,k1,1])*np.sin(c_rdata[i,j,k,1])) + (np.cos(c_rcross[i1,j1,k1,1])*np.cos(c_rdata[i,j,k,1]))*np.cos(da)
                        if dd==0.: print 'dd=0'        
                                
                        if abs(dd) > 1.:                    
                                if dd < -1.: dd=-1.
                                if dd > 1.: dd=1.
                        dd = np.arccos(dd)    
                        
                        #rp = rz*dd
                        rpl = np.log10(rz*dd)
                
                        if rpl >= rmax or rpl <= rmin: continue                    
#---------------------------------------------------------------------
# --------------------  Calculo de la Dist Radial --------------------
        
                        didl = abs(c_rdata[i,j,k,2]-c_rcross[i1,j1,k1,2])
                        #didl = difd
                                
                        if didl >= zmax or didl <= zmin: continue
#---------------------------------------------------------------------
                        
# --------------------  Bineado en rp y deltaV -----------------------         
        
                        ind = int((rpl-rmin)/dbin)  # ind y inz toman valores
                        inz = int((didl-zmin)/zbin) # entre 0 y 9
                                             
                        frr[ind,inz] += 1 
                        nrv += 1

#----------------------- Errores Jackknife----------------------------
                        
                        for l in range(10): #jackknife k=0,...,9
                            if l != njackex: 
                                rrjn[l,ind,inz] += 1
                                njnrr[l] += 1                          
#---------------------------------------------------------------------
                        
                    if jflag == 1: 
                        j1=nbins[0]
                        jflag=0
                if iflag == 1: 
                    i1=nbins[1]
                    iflag=0
                    
            if nrv > 0: nrr += 1

t2 = time.time()

print 'RanData-RanCross time elapsed:',(t2-t1)/60.,'m'

#%%

# =====================================================================
# ************* Calculo de Data-Rancross ******************
# =====================================================================

t1 = time.time()
ntr = 0
iflag=0
jflag=0
cont=0.

for i in range(nbins[1]):

    print 'DR',i,'/',nbins[1]-1
    for j in range(nbins[0]):
        
        if c_data[i,j,0,0] == 0.: continue # Revisar: no parece afectar
        for k in range(int(H_d[i,j])):
        
            z1 = c_data[i,j,k,2]+60. 
            z2 = c_data[i,j,k,2]-60.
            zc = c_data[i,j,k,2]*H_0/c 
            if  zc == 0.: 
                print 'zc=0;',i,j,k,'id:',c_rdata[i,j,k,3]                
                continue
            cont+=1.            
            ntrv = 0
            njackex = int(cont/nextt) # Jackknife
            
            
            for i1 in range(i-1,i+2):

                if i1==nbins[1]: 
                    i1 = 0
                    iflag = 1
                
                for j1 in range(j-1,j+2):
                    
                    if j1==nbins[0]: 
                        j1 = 0
                        jflag = 1
                        
                    for k1 in range(int(H_rc[i1,j1])):
                        if c_rcross[i1,j1,k1,2] >= z1 or c_rcross[i1,j1,k1,2] <= z2: continue

                        zv = c_rcross[i1,j1,k1,2]*H_0/c              
        
#-----------------------Calculo de la Dist Proy------------------------
                        zz = (zc+zv)/2.
                        rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5)))) / (H_0*(Om**2)*(1.+zz)**2)
                
                        da = abs(c_rcross[i1,j1,k1,0]-c_data[i,j,k,0])
                        if da == 0.: continue
                    
                        if da > np.pi: da = 2.*np.pi-da
                            
                        dd = (np.sin(c_rcross[i1,j1,k1,1])*np.sin(c_data[i,j,k,1])) + (np.cos(c_rcross[i1,j1,k1,1])*np.cos(c_data[i,j,k,1]))*np.cos(da)
                        if dd==0.: print 'dd=0'        
                                
                        if abs(dd) > 1.:                    
                                if dd < -1.: dd=-1.
                                if dd > 1.: dd=1.
                        dd = np.arccos(dd)    
                        
                        #rp = rz*dd
                        rpl = np.log10(rz*dd)
                
                        if rpl >= rmax or rpl <= rmin: continue                    
#---------------------------------------------------------------------
# --------------------  Calculo de la Dist Radial --------------------
        
                        didl = abs(c_data[i,j,k,2]-c_rcross[i1,j1,k1,2])
                        #didl = difd
                                
                        if didl >= zmax or didl <= zmin: continue
#---------------------------------------------------------------------
                        
# --------------------  Bineado en rp y deltaV -----------------------         
        
                        ind = int((rpl-rmin)/dbin)  # ind y inz toman valores
                        inz = int((didl-zmin)/zbin) # entre 0 y 9
                                             
                        fdr[ind,inz] +=1 
                        ntrv +=1
#----------------------- Errores Jackknife----------------------------
                        
                        for l in range(10): #jackknife l=0,...,9
                            if l != njackex: 
                                drjn[l,ind,inz] +=1
                                njndr[l] +=1                          
#---------------------------------------------------------------------                        
                    if jflag == 1: 
                        j1=nbins[0]
                        jflag=0
                if iflag == 1: 
                    i1=nbins[1]
                    iflag=0
                    
            if ntrv > 0: ntr +=1

t2 = time.time()

print 'Data-RanCross time elapsed:',(t2-t1)/60.,'m'

#%%

# ======================================================================
# -------------- Jackknife  ---------------------
# ======================================================================
ts1 = time.time()

#--------------------------------------
xjn_nat = np.zeros((10,nbdp),'float')
xjn_dp = np.zeros((10,nbdp),'float')
xjn_ham = np.zeros((10,nbdp),'float')
xjn_ls = np.zeros((10,nbdp),'float')

xm_nat = np.zeros((nbdp),'float')
xm_dp = np.zeros((nbdp),'float')
xm_ham = np.zeros((nbdp),'float')
xm_ls = np.zeros((nbdp),'float')

ejn_nat = np.zeros((nbdp),'float')
ejn_dp = np.zeros((nbdp),'float')
ejn_ham = np.zeros((nbdp),'float')
ejn_ls = np.zeros((nbdp),'float')
#--------------------------------------

#--------------------------------------
outputjnFile_nat = open('outputjn.nat_{0}'.format(sample),'w')
outputjnFile_dp = open('outputjn.dp_{0}'.format(sample),'w')
outputjnFile_ham = open('outputjn.ham_{0}'.format(sample),'w')
outputjnFile_ls = open('outputjn.ls_{0}'.format(sample),'w')
#--------------------------------------

#--------------------------------------
for k in range(10):
    djn1 = float(njnrr[k])*float(nr4)
    djn2 = float(njndd[k])*float(nd4)
    rjnorm = djn1/djn2 
    rjnorm1 = float(nr4)/float(nd4)
    rjnorm2 = float(njnrr[k])/float(njndd[k])
    rjnorm3 = float(nr4)*float(njndd[k])/(float(nd4)*float(njnrr[k]))
    
    for i in range(nbdp):
        
        for j in range(nbdv):
                
            if ddjn[k,i,j] == 0 or rrjn[k,i,j] == 0 or drjn[k,i,j] == 0: continue
            wwjn_nat = (float(ddjn[k,i,j]))/(float(rrjn[k,i,j]))
            wwjn_dp  = (float(ddjn[k,i,j]))/(float(drjn[k,i,j]))
            wwjn_ham = (float(ddjn[k,i,j]))*(float(rrjn[k,i,j]))/((float(drjn[k,i,j]))**2)
            wwjn_ls  = (float(drjn[k,i,j]))/(float(rrjn[k,i,j]))
     
            xjn_nat[k,i] = xjn_nat[k,i]+(wwjn_nat*(rjnorm)-1.)
            xjn_dp[k,i]  = xjn_dp[k,i]+(wwjn_dp*(rjnorm1)-1.)
            xjn_ham[k,i] = xjn_ham[k,i]+(wwjn_ham*rjnorm3-1.)
            xjn_ls[k,i]  = xjn_ls[k,i]+(wwjn_nat*(rjnorm)-2.*(rjnorm2)*wwjn_ls+1.)
                        
        xjn_nat[k,i] = 2.*xjn_nat[k,i]*zbin 
        xjn_dp[k,i]  = 2.*xjn_dp[k,i]*zbin            
        xjn_ham[k,i] = 2.*xjn_ham[k,i]*zbin            
        xjn_ls[k,i]  = 2.*xjn_ls[k,i]*zbin      

for i in range(nbdp):
    
    for k in range(10):
        xm_nat[i] = xm_nat[i]+xjn_nat[k,i] 
        xm_dp[i]  = xm_dp[i]+xjn_dp[k,i] 
        xm_ham[i] = xm_ham[i]+xjn_ham[k,i] 
        xm_ls[i]  = xm_ls[i]+xjn_ls[k,i] 

    xm_nat[i] = xm_nat[i]/10.
    xm_dp[i]  = xm_dp[i]/10.
    xm_ham[i] = xm_ham[i]/10.
    xm_ls[i]  = xm_ls[i]/10.
        
for i in range(nbdp):
    rp = rmin+dbin/2.*(2.*float(i+1)-1.)
    rp = 10.**rp
    
    for k in range(10):
        ejn_nat[i] = ejn_nat[i]+((xjn_nat[k,i]-xm_nat[i])**2)
        ejn_dp[i]  = ejn_dp[i]+((xjn_dp[k,i]-xm_dp[i])**2)
        ejn_ham[i] = ejn_ham[i]+((xjn_ham[k,i]-xm_ham[i])**2)
        ejn_ls[i]  = ejn_ls[i]+((xjn_ls[k,i]-xm_ls[i])**2)
        
        outputjnFile_nat.write(str(k)+' '+str(rp)+' '+str(xm_nat[i])+' '+str(xjn_nat[k,i])+'\n')
        outputjnFile_dp.write(str(k)+' '+str(rp)+' '+str(xm_dp[i])+' '+str(xjn_dp[k,i])+'\n')
        outputjnFile_ham.write(str(k)+' '+str(rp)+' '+str(xm_ham[i])+' '+str(xjn_ham[k,i])+'\n')
        outputjnFile_ls.write(str(k)+' '+str(rp)+' '+str(xm_ls[i])+' '+str(xjn_ls[k,i])+'\n')
        
    ejn_nat[i]= np.sqrt(ejn_nat[i]/10.)
    ejn_dp[i]= np.sqrt(ejn_dp[i]/10.)
    ejn_ham[i]= np.sqrt(ejn_ham[i]/10.)
    ejn_ls[i]= np.sqrt(ejn_ls[i]/10.)

        
outputjnFile_nat.close()
outputjnFile_dp.close()
outputjnFile_ham.close()
outputjnFile_ls.close()
#--------------------------------------

ts2 = time.time()

print t2-t1

#%%
t1 = time.time()

# ======================================================================
# -------------- Calculo la Funcion de Correlacion ---------------------
# ======================================================================

# ///Comentariar si se quiere usar datos calculados ahora mismo///
# ------------

#sample='blue'
#
#if sample=='blue': nrr,ntt,nd4,nr4=11519,1151,211472,211700
#if sample=='red': nrr,ntt,nd4,nr4=1038,1061,211472,211700

# -----------
# /////


xi_nat = np.zeros((bins),'float')
xi_dp  = np.zeros((bins),'float')
xi_ham = np.zeros((bins),'float')
xi_ls  = np.zeros((bins),'float')

ww_nat = np.zeros((nbdp,nbdv),'float')
ww_dp  = np.zeros((nbdp,nbdv),'float')
ww_ham = np.zeros((nbdp,nbdv),'float')
ww_ls  = np.zeros((nbdp,nbdv),'float')

dn1 = float(nrr)*float(nr4)
dn2 = float(ntt)*float(nd4)
rnorm = dn1/dn2
rnorm1 = float(nr4)/float(nd4)
rnorm2 = float(nrr)/float(ntt)
rnorm3 = float(nr4)*float(ntt)/(float(nd4)*float(nrr))

outputfsFile = open('output_hist2d.fs_{0}.txt'.format(sample),'w')



#--------------
"""
Solo para cuando ya estan calculados los fdd,frr,fdr y quiero ver graficos
"""
#fdd = np.zeros((nbdp,nbdv),'int')  
#frr = np.zeros((nbdp,nbdv),'int')  
#fdr = np.zeros((nbdp,nbdv),'int')  
#
#readfsFile = open('../ProgramasGeorgi/CorrelationFunction/fede/output_hist2d.fs_{0}.txt'.format(sample),'r')
#
#line = readfsFile.read().split('\n')
#
#for var in line:
#                
#    if var == '': break
#    
#    sepList = var.split()   
#    i=float(sepList[0])
#    j=float(sepList[1])
#    fdd[i,j]=float(sepList[2])
#    frr[i,j]=float(sepList[3])
#    fdr[i,j]=float(sepList[4])
#
#readfsFile.close()
#---------

outputprFile_nat = open('output_hist2d.pr.nat_{0}'.format(sample),'w')
outputprFile_dp  = open('output_hist2d.pr.dp_{0}'.format(sample),'w')
outputprFile_ham = open('output_hist2d.pr.ham_{0}'.format(sample),'w')
outputprFile_ls  = open('output_hist2d.pr.ls_{0}'.format(sample),'w')

for i in range(nbdp):
    rpf[i] = rmin+dbin/2.*(2.*float(i+1)-1.) # Codigo en Fortran dice float(i)
    rpf[i] = 10.**rpf[i]
            
    for j in range(nbdv):
            
        if fdd[i,j] == 0. or frr[i,j] == 0. or fdr[i,j] == 0.: continue
        ww_nat = (float(fdd[i,j]))/(float(frr[i,j]))
        ww_dp  = (float(fdd[i,j]))/(float(fdr[i,j]))
        ww_ham = (float(fdd[i,j]))*(float(frr[i,j]))/((float(fdr[i,j]))**2)
        ww_ls  = (float(fdr[i,j]))/(float(frr[i,j]))
 
        xi_nat[i] = xi_nat[i]+(ww_nat*(rnorm)-1.)
        xi_dp[i]  = xi_dp[i]+(ww_dp*(rnorm1)-1.)
        xi_ham[i] = xi_ham[i]+(ww_ham*rnorm3-1.)
        xi_ls[i]  = xi_ls[i]+(ww_nat*(rnorm)-2.*(rnorm2)*ww_ls+1.)
        
        outputfsFile.write(str(i)+' '+str(j)+' '+str(fdd[i,j])+' '+str(frr[i,j])+' '+str(fdr[i,j])+'\n')
        
    xi_nat[i] = 2.*xi_nat[i]*zbin
    xi_dp[i]  = 2.*xi_dp[i]*zbin
    xi_ham[i] = 2.*xi_ham[i]*zbin
    xi_ls[i]  = 2.*xi_ls[i]*zbin
    
    outputprFile_nat.write(str(rpf[i])+' '+str(xi_nat[i])+' '+str(ejn_nat[i])+'\n')
    outputprFile_dp.write(str(rpf[i])+' '+str(xi_dp[i])+' '+str(ejn_dp[i])+'\n')
    outputprFile_ham.write(str(rpf[i])+' '+str(xi_ham[i])+' '+str(ejn_ham[i])+'\n')
    outputprFile_ls.write(str(rpf[i])+' '+str(xi_ls[i])+' '+str(ejn_ls[i])+'\n')
    
outputfsFile.close()
outputprFile_nat.close()
outputprFile_dp.close()
outputprFile_ham.close()
outputprFile_ls.close()

t2 = time.time()

print t2-t1

t100=time.time()
#%%
print 'Total time elapsed:', (t100-t0)/60.,'m'
timeFile.write('Nbins='+str(nbins)+'. Total time elapsed:'+str((t100-t0)/60.)+'m'+'\n')
timeFile.close()

#%%
import graf_ws

graf_ws.plot(sample,rpf,nbins,xi_nat,xi_dp,xi_ls,xi_ham,ejn_nat,ejn_dp,ejn_ls,ejn_ham,save=True)
