#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 22 15:26:45 2017

@author: fdavilakurban
"""
#%%
print 'Reading Data' 

rhoEstPares12 = ascii.read('fixap_pares_1.0-2.0-1000_est.txt',format='no_header')
rhoEstPares23 = ascii.read('fixap_pares_2.0-3.0-1000_est.txt',format='no_header')
rhoEstPares34 = ascii.read('fixap_pares_3.0-4.0-1000_est.txt',format='no_header')
rhoEstPares45 = ascii.read('fixap_pares_4.0-5.0-1000_est.txt',format='no_header')

rhoEstTripletes12 = ascii.read('fixap_tripletes_1.0-2.0-1000_est.txt',format='no_header')
rhoEstTripletes23 = ascii.read('fixap_tripletes_2.0-3.0-1000_est.txt',format='no_header')
rhoEstTripletes34 = ascii.read('fixap_tripletes_3.0-4.0-1000_est.txt',format='no_header')
rhoEstTripletes45 = ascii.read('fixap_tripletes_4.0-5.0-1000_est.txt',format='no_header')

rhoEstGmenores12 = ascii.read('fixap_gmenores_1.0-2.0-1000_est.txt',format='no_header')
rhoEstGmenores23 = ascii.read('fixap_gmenores_2.0-3.0-1000_est.txt',format='no_header')
rhoEstGmenores34 = ascii.read('fixap_gmenores_3.0-4.0-1000_est.txt',format='no_header')
rhoEstGmenores45 = ascii.read('fixap_gmenores_4.0-5.0-1000_est.txt',format='no_header')

#%%
plt.ylim((-2.,8.))
plt.title('Ap 1-2Mpc')
plt.boxplot([rhoEstPares12['col1'],rhoEstTripletes12['col1'],rhoEstGmenores12['col1']]
            ,labels=['Pairs','Triplets','Minor Groups'],notch='True',bootstrap=1000)
plt.show()

plt.ylim((-2.,8.))
plt.title('Ap 2-3Mpc')
plt.boxplot([rhoEstPares23['col1'],rhoEstTripletes23['col1'],rhoEstGmenores23['col1']]
            ,labels=['Pairs','Triplets','Minor Groups'],notch='True',bootstrap=1000)
plt.show()

plt.ylim((-2.,8.))
plt.title('Ap 3-4Mpc')
plt.boxplot([rhoEstPares34['col1'],rhoEstTripletes34['col1'],rhoEstGmenores34['col1']]
            ,labels=['Pairs','Triplets','Minor Groups'],notch='True',bootstrap=1000)
plt.show()

plt.ylim((-2.,8.))
plt.title('Ap 4-5Mpc')
plt.boxplot([rhoEstPares45['col1'],rhoEstTripletes45['col1'],rhoEstGmenores45['col1']]
            ,labels=['Pairs','Triplets','Minor Groups'],notch='True',bootstrap=1000)
plt.show()


#%%

##%%
#
#print 'Calculating Means'
#
#meanPares12 = np.mean(rhoEstPares12['col1'])
#meanPares23 = np.mean(rhoEstPares23['col1'])
#meanPares34 = np.mean(rhoEstPares34['col1'])
#meanPares45 = np.mean(rhoEstPares45['col1'])
#
#meanTripletes12 = np.mean(rhoEstTripletes12['col1'])
#meanTripletes23 = np.mean(rhoEstTripletes23['col1'])
#meanTripletes34 = np.mean(rhoEstTripletes34['col1'])
#meanTripletes45 = np.mean(rhoEstTripletes45['col1'])
#
#meanGmenores12 = np.mean(rhoEstGmenores12['col1'])
#meanGmenores23 = np.mean(rhoEstGmenores23['col1'])
#meanGmenores34 = np.mean(rhoEstGmenores34['col1'])
#meanGmenores45 = np.mean(rhoEstGmenores45['col1'])
#
##%%
#
#print 'Calculating Bootstrap errors' 
#
#def bootstrap(data, num_samples):
#    n = len(data)
#    idx = np.random.randint(0, n, (num_samples, n))
#    samples = data[idx]
#    stat = (np.median(samples, 1))
#    return np.std(stat)
#
#meanPares12_err = bootstrap(rhoEstPares12['col1'],100)
#meanPares23_err = bootstrap(rhoEstPares23['col1'],100)
#meanPares34_err = bootstrap(rhoEstPares34['col1'],100)
#meanPares45_err = bootstrap(rhoEstPares45['col1'],100)
#
#meanTripletes12_err = bootstrap(rhoEstTripletes12['col1'],100)
#meanTripletes23_err = bootstrap(rhoEstTripletes23['col1'],100)
#meanTripletes34_err = bootstrap(rhoEstTripletes34['col1'],100)
#meanTripletes45_err = bootstrap(rhoEstTripletes45['col1'],100)
#
#meanGmenores12_err = bootstrap(rhoEstGmenores12['col1'],100)
#meanGmenores23_err = bootstrap(rhoEstGmenores23['col1'],100)
#meanGmenores34_err = bootstrap(rhoEstGmenores34['col1'],100)
#meanGmenores45_err = bootstrap(rhoEstGmenores45['col1'],100)
#
