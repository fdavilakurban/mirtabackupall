

#galf = 'SCG_centros_maskedallgxs_Compactos.dat'
#galf = 'SCG_vecinos_final_masked_Compactos_gr_C_rpmax.dat'
galf = 'SCG_centros_paper_nuevo.dat'
#galf = 'SCG_vecinos_paper.dat'

fr = fast_reader={'parallel': True, 'use_fast_converter': True}
print 'Reading file: ', galf
gals = ascii.read(galf,delimiter=' ',fast_reader=fr,fill_values=('--'))#,'','zprom'),('-99.99','','sigmaz')])
print '--> nr of rows : ', len(gals)

#%%
#import numpy as np
#a=np.where(gals['zprom']=='')
#gals.remove_rows(a)

#%%
#ascii.write(gals,'SCG_centros_paper_clean.dat',format='commented_header',names=gals.colnames)
#gals = ascii.read('SCG_centros_paper_clean.dat',delimiter=' ',fast_reader=fr)
#%%
#x=np.where(gals['zprom']>=.15)
#gals.remove_rows(x)
#print 'Final nr of center rows : ', len(gals)

#%%
gpar = gals[gals['Nm']==2]
gtri = gals[gals['Nm']==3]
gm = gals[gals['Nm']>=4]

#gm4 = gals[gals['Nm']==4]
#gm5 = gals[gals['Nm']==5]

#gm45 = gals[(gals['Nm']==4) | (gals['Nm']==5)]

#gm6 = gals[gals['Nm']==6]

#gm56 = gals[(gals['Nm']==5) | (gals['Nm']==6)]

#%%
#par1 = gpar[(2.*gpar['rpmax'])<=25.]
#%%
#par2 = gpar[2.*gpar['rpmax(fromcent)']>25.]
#par2 = par2[2.*par2['rpmax(fromcent)']<=50.]

#par3 = gpar[2.*gpar['rpmax(fromcent)']<50.]
#par3 = par3[2.*par3['rpmax(fromcent)']<=100.]

#par4 = gpar[2.*gpar['rpmax(fromcent)']>=100.]

parc = gpar[2.*gpar['rpmax(fromcent)']<=100.]
#pard = gpar[2.*gpar['rpmax(fromcent)']>100.]

#%%
#ascii.write(gals,'SCG_centros_maskedallgxs_Compactos_clean.dat',format='commented_header',names=gals.colnames)
ascii.write(gpar,'pares.txt',format='commented_header')
#ascii.write(par1,'pares1.txt',format='commented_header')
#ascii.write(par2,'pares2.txt',format='commented_header')
#ascii.write(par3,'pares3.txt',format='commented_header')
#ascii.write(par4,'pares4.txt',format='commented_header')
ascii.write(parc,'paresc.txt',format='commented_header')
#ascii.write(pard,'paresd.txt',format='commented_header')

ascii.write(gtri,'tripletes.txt',format='commented_header')

ascii.write(gm,'gmenores.txt',format='commented_header')
#ascii.write(gm45,'gm45.txt',format='commented_header')
#ascii.write(gm56,'gm56.txt',format='commented_header')
#ascii.write(gm4,'gm4.txt',format='commented_header')
#ascii.write(gm5,'gm5.txt',format='commented_header')
#ascii.write(gm6,'gm6.txt',format='commented_header')
