# -*- coding: utf-8 -*-
"""
Created on Thu Jun  2 12:10:08 2016

@author: vvvteam
"""
Om = 0.3
c = 300000.
H_0 = 70.

cosmo = LambdaCDM(H_0,Om,Ode0=0.7)

#converters = {'Xcent': [ascii.convert_numpy(np.float64)]}

scg = ascii.read('SCG_centros_paper.dat')#,converters=converters)

hdulist = fits.open('MGS_DR10_maskedIDgr.fits',memmap=True)
cross = Table(hdulist[1].data)
cross.rename_column('ra_1','ra')
cross.rename_column('dec_1','dec')

x=np.where(cross['Mr']>=-19.)
cross.remove_rows(x)

#%%
scg['Xcent'] *= np.pi/180. # a radianes
scg['Ycent'] *= np.pi/180.

cross['ra'] *= np.pi/180. # a radianes
cross['dec']*= np.pi/180.

X=[]
Y=[]
Z=[]
for i in range(len(scg)):
    print 'Data Coordinates Conversion:',i,'/',len(scg)-1
    d = cosmo.comoving_transverse_distance(scg['zprom'][i]).value # distancia co-movil
    #d = 1.
    X.append( d * np.cos(scg['Xcent'][i]) * np.cos(scg['Ycent'][i]) )
    Y.append( d * np.sin(scg['Xcent'][i]) * np.cos(scg['Ycent'][i]) )
    Z.append( d * np.sin(scg['Ycent'][i]) )

col_x = Column(name='X',data=X)   
col_y = Column(name='Y',data=Y)   
col_z = Column(name='Z',data=Z)   

scg.add_columns((col_x,col_y,col_z))

#%%
X=[]
Y=[]
Z=[]
for i in range(len(cross)):
	t1 = time.time()
	print 'Cross Coordinates Conversion:',i,'/',len(cross)-1
	d = cosmo.comoving_transverse_distance(cross['z'][i]).value # distancia co-movil
	#d = 1.
	X.append( d * np.cos(cross['ra'][i]) * np.cos(cross['dec'][i]) )
	Y.append( d * np.sin(cross['ra'][i]) * np.cos(cross['dec'][i]) )
	Z.append( d * np.sin(cross['dec'][i]) )
	print time.time()-t1
col_x = Column(name='X',data=X)   
col_y = Column(name='Y',data=Y)   
col_z = Column(name='Z',data=Z)   

cross.add_columns((col_x,col_y,col_z))
