# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 22:49:55 2015

Asignar z a glxs random

@author: federico
"""
#%%

c = 300000.
H_0 = 70.

#percent = 10
sample = 'MGS_DR10_maskedIDgr.fits'
ransample = 'random_MGS_DR10.fits'

#%%
import time
t1=time.time()

print 'Reading file: ', ransample
hdulist = fits.open(ransample,memmap=True)
rancat = Table(hdulist[1].data)
print '--> nr of rows : ', len(rancat)

rancat.remove_column('id_pixel')

#%%

galf  = '{}'.format(sample) # data sample
if sample == 'todos': galf = 'SCG_centros_maskedallgxs_Compactos_clean.dat'
#fr = fast_reader={'parallel': True, 'use_fast_converter': True}
print 'Reading file: ', galf
#gals = ascii.read(galf,delimiter=' ',fast_reader=fr)
hdulist = fits.open(sample,memmap=True)
gals = Table(hdulist[1].data)
print '--> nr of rows : ', len(gals)

#gals['sigmaz']/=c
#gals['z']/=c

#%%
from astropy.table import Column

rancat.remove_rows(slice(len(gals),len(rancat)))

colz = Column(gals['z'],name='z')
rancat.add_column(colz)


#%%
#print 'Creating random list'
#rand = [rancat[i].as_void() for i in random.sample(xrange(len(rancat)),len(gals))]
##rand = Table(rand)
#print '--> nr of rows : ', len(rand)


#%%
#z=[]
##sigmazmed=np.median(gals['sigmaz'][gals['nspec']==2])
#
#
#for i in range(len(rand)):
#    gal=random.choice(gals)
#    zprom,sigma=gal['zprom'],gal['sigmaz']
##    if sigma==-99.99/c:
##        z.append(zprom)
#    zprom1=random.gauss(zprom,sigma)
#    z.append(zprom1)

#%%
#rannew=np.zeros((len(rand),3))
#
#for i in range(len(rand)):
#    rannew[i]=rand[i][0],rand[i][1],z[i]
#
#rannew = Table(rannew,names=['ra','dec','z'])
#%%
ascii.write(rancat,'random_DR10.dat',format='commented_header')

t2=time.time()

print t2-t1
#%%
#import matplotlib.pyplot as plt
#from scipy import stats
#
##a=Table(rannew)
#bins=25
#plt.close()
#
#ks,pv=stats.ks_2samp(gals['zprom'],z)
#print ks,pv
#
#plt.hist(gals['zprom'], bins=bins, histtype='stepfilled', normed=True, color='b',label=sample)
#plt.hist(z, bins=bins, histtype='stepfilled', normed=True, color='r', alpha=.5,label='random')
#
#plt.title("Redshift Distribution")
#plt.xlabel("z")
##plt.ylabel()
#plt.legend()
#plt.show()    


