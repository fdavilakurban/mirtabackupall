# -*- coding: utf-8 -*-
"""
Created on Tue May 31 09:56:17 2016


@author: vvvteam
"""
sigma=5

# ASEGURARSE DE HABER CORRIDO coordconv.py PARA TRABAJAR EN LAS
# COORDENADAS CORRECTAS

#%%
#scg = ascii.read('SCG_centros_paper.dat')#,converters=converters)

pares = scg[scg['Nm']==2]
tripletes = scg[scg['Nm']==3]
gmenores = scg[scg['Nm']>=4]

sigma_p = []
sigma_t = []
sigma_gm = []

crossPos = np.column_stack((cross['X'],cross['Y'],cross['Z']))
cposTree = spatial.cKDTree(crossPos)

#%%
for sample in [pares,tripletes,gmenores]:
	dataPos = np.column_stack((sample['X'],sample['Y'],sample['Z'])) 
	N = sample['Nm'][0]
	
	if N >= 4:	
		idx = cposTree.query(dataPos,k=sigma+10) 
	else:
		idx = cposTree.query(dataPos,k=sigma+N) # '+N' porque va a encontrar los miembros de su propio grupo

	for i in range(len(sample)):
	    dist = idx[0][i][sigma + sample['Nm'][i] - 1.]
	    
	    if N == 2: sigma_p.append( np.log10(float(sigma) / (4./3.*np.pi*(dist**3))) )
	    if N == 3: sigma_t.append( np.log10(float(sigma) / (4./3.*np.pi*(dist**3))) )
	    if N >= 4: sigma_gm.append( np.log10(float(sigma) / (4./3.*np.pi*(dist**3))) )
 
plt.hist(sigma_p,bins=20,linewidth=1.8,linestyle='solid',histtype='step',normed=True,label='Pairs')
plt.hist(sigma_t,bins=18,linewidth=1.8,linestyle='dashed',histtype='step',normed=True,label='Triplets')
plt.hist(sigma_gm,bins=12,linewidth=1.8,linestyle='dotted',histtype='step',normed=True,label='Groups')
plt.xlabel('$\log (\Sigma{})$'.format(sigma),fontsize=18)

plt.legend(fancybox=True,frameon=True,shadow=True,loc=1)
    
plt.show()
#%%
plt.savefig('sigma{}_paper.pdf'.format(sigma))
#%%
execfile('sigmaMedian.py')










#%%

idx = cposTree.query(dataPos[13],k=20)
print cross[idx[1]]['groupID']
#a,b,c,a1=cross[idx[1]]['groupID'],cross[idx[1]]['ra'],cross[idx[1]]['dec'],cosmo.comoving_distance(cross[idx[1]]['z']).value
#d,e,f,g=gmenores['name'][1],gmenores['Xcent'][0],gmenores['Ycent'][0], cosmo.comoving_distance(gmenores['zprom'][0]).value
