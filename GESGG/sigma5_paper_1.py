"""
10/04/2017

Nuevo intento de hacer NN-search. Buscando un punto medio entre eficacia y robustez.
"""

scg = ascii.read('SCG_centros_paper.dat')#,converters=converters)

hdulist = fits.open('MGS_DR10_maskedIDgr.fits',memmap=True)
cross = Table(hdulist[1].data)
cross.rename_column('ra_1','ra')
cross.rename_column('dec_1','dec')

x=np.where(cross['Mr']>=-19.)
cross.remove_rows(x)

scg['Xcent'] *= np.pi/180. # a radianes
scg['Ycent'] *= np.pi/180.

cross['ra'] *= np.pi/180. # a radianes
cross['dec']*= np.pi/180.
#---------------------------------------------------------------------

pares = scg[scg['Nm']==2]
tripletes = scg[scg['Nm']==3]
gmenores = scg[scg['Nm']>=4]

sigma_p = []
sigma_t = []
sigma_gm = []

crossPos = np.column_stack((cross['ra'],cross['dec'],cross['z']))
cposTree = spatial.cKDTree(crossPos)
#---------------------------------------------------------------------

for sample in pares:
	dataPos = np.column_stack((sample['Xcent'],sample['Ycent'],sample['zprom'])) 
