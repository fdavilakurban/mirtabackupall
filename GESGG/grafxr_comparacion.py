# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 09:44:16 2015

@author: vvvteam
"""

from astropy.io import ascii
#from astropy.table import Table
import matplotlib.pyplot as plt
#from pylab import rcParams
#rcParams['figure.figsize'] = 15, 10


#plt.figure(1)
#plt.subplot(411)
estimator = 'ls'

par = ascii.read('output_hist2d.{}_paresc.rev.txt'.format(estimator),delimiter=' ')
tri = ascii.read('output_hist2d.{}_tripletes.rev.txt'.format(estimator),delimiter=' ')
gm = ascii.read('output_hist2d.{}_gmenores.rev.txt'.format(estimator),delimiter=' ')

par.remove_row(0)
tri.remove_row(0)
gm.remove_row(0)
#%%
#from scipy.optimize import leastsq as lsq
#import numpy as np
#def func(x,a,b):
#    return a*x+b
#x0=np.array([-.1,10**2.1])    
#print lsq(func,x0,args=(par['col1'].data,par['col2'].data))
#    



#%%
plt.close()

plt.errorbar(par['col1'], par['col2'], yerr=par['col3'], color='k', fmt='')
plt.scatter(par['col1'], par['col2'], color='k', s=25, label='Pares')

plt.errorbar(tri['col1'], tri['col2'], yerr=tri['col3'], color='r', fmt='')
plt.scatter(tri['col1'], tri['col2'], color='r', s=25, label='Tripletes')

plt.errorbar(gm['col1'], gm['col2'], yerr=gm['col3'], color='b', fmt='')
plt.scatter(gm['col1'], gm['col2'], color='b', s=25, label='Grupos Menores')





plt.grid()
plt.xlabel('$r\,(Mpc\,h^{-1})$',fontsize='x-large')
plt.ylabel('$\\xi(r)$',fontsize='xx-large')
#plt.title('FC ({}) - Todos los Sistemas'.format(estimator),fontsize='x-large')
plt.legend(fancybox=True,shadow=True,frameon=True,fontsize=25)

plt.xlim(10.**-.4, 10.**1.15) 
plt.ylim(10**-1, 10.**1.5)
plt.yscale('log')
plt.xscale('log')
plt.tight_layout()
#plt.savefig('graf_xr_{}.pdf'.format(estimator))
plt.show()