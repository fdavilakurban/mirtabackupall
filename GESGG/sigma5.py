# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 12:09:25 2015

SIGMA5

@author: federico
"""

#%%
import numpy as np
import time
#import datetime

#%%

#===Parameters==========================

Om = 0.3
c = 300000.
H_0 = 70.
pr = np.pi/180.


rmax = np.log10(20.)
rmin = np.log10(0.15)

zmax = 1000./H_0 # (dV/c)*(c/H_0)=dz[Mpc]
zmin = 0.0001


#%%

t1 = time.time()

from astropy.io import ascii

sample = 'pares1'

galf  = '{}.txt'.format(sample) # data sample
calf  = 'MGS_DR10_masked.cross' # cross sample

# READ INPUT DATA (fast!) into astropy tables
#cols = ['ra','dec','z']#,'apm','abm','zmin','zmax','wei']
fr = fast_reader={'parallel': True, 'use_fast_converter': True}
print 'Reading file: ', galf
gals = ascii.read(galf,delimiter=' ',fast_reader=fr)
print '--> nr of rows : ', len(gals)
print 'Reading file: ', calf
cals = ascii.read(calf,delimiter=' ',fast_reader=fr)
print '--> nr of rows : ', len(cals)


t2 = time.time()

print 'Time spent reading files: ',t2-t1,'s'

nt=len(gals)

#%%
t1=time.time()
a1=[]
d1=[]

rpmax = 20. # r max de la fc 
zz = .06 # z min de la muestra
rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5)))) / (H_0*(Om**2)*(1.+zz)**2)

for a,d,z in gals:
    a1.append(float(a))
    d1.append(float(d))
a1.sort()
d1.sort()

t2=time.time()
print 'Time spent calculating bins:',t2-t1,'s'

nbins=int((a1[-1]-a1[0])/(rpmax/rz)),int((d1[-1]-d1[0])/(rpmax/rz))

print 'Nbins=',nbins
#%%
print 'Creating the coordenates matrices...'
t1=time.time()
import hist2drec_fede
#nbins=40,10
c_data,H_d=hist2drec_fede.hist2d_fede(gals,nbins)#,plot=True,plttitle='Data')
print 'Data Matrix created'
c_cross,H_c=hist2drec_fede.hist2d_fede(cals,nbins)#,plot=True,plttitle='Cross') 
print 'Cross Matrix created'
t2=time.time()

print 'Time spent creating the matrices: ',(t2-t1)/60.,'m'

#%%
t1 = time.time()
ntt = 0
iflag=0
jflag=0
r5=[]
rp=np.zeros((nbins[1],nbins[0],H_d.max(),H_c.max()),'float')
for i in range(nbins[1]):
    
    #print i,'/',nbins[1]
    for j in range(nbins[0]):
        print 'i,j',i,j
        #if c_data[i,j,0,0] == 0.: continue # Revisar: no parece afectar
        #rp=[]        
        for k in range(int(H_d[i,j])):
            z1 = c_data[i,j,k,2]+zmax
            z2 = c_data[i,j,k,2]-zmax
            zc = c_data[i,j,k,2]*H_0/c 
                        
            for i1 in range(i-1,i+2):

                if i1==nbins[1]: 
                    i1 = 0
                    iflag = 1
                
                for j1 in range(j-1,j+2):
                    #print 'i1,j1',i1,j1
                    if j1==nbins[0]: 
                        j1 = 0
                        jflag = 1
                        
                    for k1 in range(int(H_c[i1,j1])):
                        #print i1,j1,k1
                        if c_cross[i1,j1,k1,2] >= z1 or c_cross[i1,j1,k1,2] <= z2: continue
                        zv = c_cross[i1,j1,k1,2]*H_0/c              
        
#-----------------------Calculo de la Dist Proy------------------------
                        zz = (zc+zv)/2.
                        rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5)))) / (H_0*(Om**2)*(1.+zz)**2)
                        if rz==0.: 
                            print 'rz=0'                            
                            continue
                        if rz <=0.1: print 'rz chico'    
                        da = abs(c_cross[i1,j1,k1,0]-c_data[i,j,k,0])
                    
                        if da > np.pi: da = 2.*np.pi-da
                        if da == 0.: continue
                            
                        dd = (np.sin(c_cross[i1,j1,k1,1])*np.sin(c_data[i,j,k,1])) + (np.cos(c_cross[i1,j1,k1,1])*np.cos(c_data[i,j,k,1]))*np.cos(da)
                        if dd==0.: print 'dd=0'        
                        if abs(dd) > 1.:                    
                                if dd < -1.: dd=-1.
                                if dd > 1.: dd=1.
                        dd = np.arccos(dd)    
                        
                        rp[i,j,k,k1]=rz*dd

                    if jflag == 1: 
                        j1=nbins[0]
                        jflag=0
#---------------------------------------------------------------------
                if iflag == 1: 
                    i1=nbins[1]
                    iflag=0
#%%                
r5=[]
for i in range(nbins[1]):
    for j in range(nbins[0]):
        for k in range(int(H_d[i,j])):
            rp[i,j,k].sort()
            #if rp[i,j,k,0]==0.:continue            
            aux=np.trim_zeros(rp[i,j,k])
            if np.size(aux)==0:continue            
            print i,j,k
            r5.append(np.log10(5./(np.pi*(aux[4])**2)))
#%%
r5File = open('../ProgramasGeorgi/CorrelationFunction/fede/sigma5_blue.txt','w')

for s in r5:
    r5File.write(str(s)+'\n')
r5File.close()
        