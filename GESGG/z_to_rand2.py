# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 22:49:55 2015

Asignar z a glxs random

@author: federico
"""
#%%

c = 300000.
H_0 = 100.

percent = 10 # Qué % mayor será la muestra aleatoria generada respecto de la real 
sample = 'paresc' # Muestra real
ransample = 'random_MGS_DR10.fits' # Catálogo aleatorio

#%% Lectura de Archivos
t1=time.time()

print 'Reading file: ', ransample
hdulist = fits.open(ransample,memmap=True)
rancat = Table(hdulist[1].data)
print '--> nr of rows : ', len(rancat)
rancat.remove_column('id_pixel')
#%% (Para el archivo de Trazadores)

#print 'Reading file: ', sample
#hdulist = fits.open(sample,memmap=True)
#gals = Table(hdulist[1].data)
#
#x=np.where(gals['Mr']>=-19.)
#gals.remove_rows(x)
#
## Mas y menos 60Mpc respecto del minimo y maximo redshift de los Centros
#
#z1=.05-60*H_0/c
#x=np.where(gals['z']<=z1)
#gals.remove_rows(x)
#
#z2=.15+60*H_0/c
#x=np.where(gals['z']>=z2)
#gals.remove_rows(x)
#
#print '--> nr of rows : ', len(gals)

#%% Lectura de Archivos
galf  = '{}.txt'.format(sample) # data sample
if sample == 'todos': galf = 'SCG_centros_maskedallgxs_Compactos_clean.dat'

fr = fast_reader={'parallel': True, 'use_fast_converter': True}
print 'Reading file: ', galf
gals = ascii.read(galf,delimiter=' ',fast_reader=fr)#,fill_values=('-99.99','nan','sigmaz'))
print '--> nr of rows : ', len(gals)

gals['sigmaz']/=c
#%% Creación de la Lista Aleatoria
print 'Creating random list'
rand = [rancat[i].as_void() for i in ran.sample(xrange(len(rancat)),percent*len(gals))]
#rand = Table(rand)
print '--> nr of rows : ', len(rand)

#%% Asignación de Redshifts
z=[]
z1=gals['zprom'].min()
z2=gals['zprom'].max()
#sigma=30/c # Error en la medicion de z de gxs de la MGS

while len(z)<len(rand):
    gal=random.choice(gals)
    zprom,sigma=gal['zprom'],gal['sigmaz']
    zprom=gal['zprom']
    if sigma==-99.99/c:
        z.append(zprom)
    zprom1=random.gauss(zprom,sigma)
    if zprom1<z1 or zprom1>z2: continue
    z.append(zprom1)

#%% 
rannew=np.zeros((len(rand),4))

for i in range(len(rand)):
    rannew[i]=rand[i][0],rand[i][1],z[i],0
#%% Escritura del Catálogo Aleatorio Generado
ascii.write(rannew,'random_{}.txt'.format(sample),names=['ra','dec','z','groupID'],format='commented_header')
print 'random_{}.txt'.format(sample),'created'

t2=time.time()

print t2-t1
#%% Histograma de la Muestra Real y la Aleatoria - Resultados del ks-test
import matplotlib.pyplot as plt
from scipy import stats

a=Table(rannew)
bins=15
plt.close()

ks,pv=stats.ks_2samp(gals['zprom'],a['col2'])
print ks,pv

plt.hist(gals['zprom'], bins=bins, histtype='step', linewidth=1.8, normed=True, color='b',label='Pares')
plt.hist(a['col2'], bins=bins, histtype='step', linewidth=1.8, normed=True, color='r',label='Random Pares')

#plt.title()
plt.xlabel('$z$',fontsize=20)
plt.ylabel('Distr. Normalizada',fontsize=15)
plt.legend(fancybox=True,frameon=True,shadow=True,loc=1,fontsize=11)
plt.show()    
plt.savefig('z_randomgm-paresc.png')

