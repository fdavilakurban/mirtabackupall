# -*- coding: utf-8 -*-
"""
Created on Sun Nov 15 11:25:54 2015

@author: federico
"""
#%%
from astropy.io import ascii
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.table import Table

#%%
#sample1 = 'gmenores'
#sample2 = 'gm4'
#sample3 = 'gm5'
#sample4 = 'gm6'
#
#
#galf1 = '{}.txt'.format(sample1)
#galf2 = '{}.txt'.format(sample2)
#galf3 = '{}.txt'.format(sample3)
#galf4 = '{}.txt'.format(sample4)

galf1='MGS_DR10_maskedIDgr.fits' 
galf2='random_MGS_DR10.fits' 


hdulist = fits.open(galf1,memmap=True)
gals1 = Table(hdulist[1].data)

hdulist = fits.open(galf2,memmap=True)
gals2 = Table(hdulist[1].data)


#fr = fast_reader={'parallel': True, 'use_fast_converter': True}
#gals2 = ascii.read(galf2,delimiter=' ',fast_reader=fr,format='no_header')#,fill_values=('nan','-3.'))
#%%
#gals2.rename_column('Xcent','ra')
#gals2.rename_column('Ycent','dec')
#gals2.rename_column('zprom','z')

for name in gals1.colnames:
    if name == 'ra_1':
        gals1.rename_column('ra_1','ra')
        gals1.rename_column('dec_1','dec')
for name in gals2.colnames:
    if name == 'ra_1':
        gals2.rename_column('ra_1','ra')
        gals2.rename_column('dec_1','dec')
        

#%%
#from pylab import rcParams
plt.close()
#rcParams['figure.figsize'] = 15, 10

#plt.errorbar(rp, xi_nat, yerr=ejn_nat, color='k', fmt='')
plt.scatter(gals1['ra'],gals1['dec'],color='k',label='Random - MGS',s=10)

#plt.errorbar(rp, xi_dp, yerr=ejn_dp, color='r', fmt='')
plt.scatter(gals2['col1'],gals2['col2'],color='r',label='Random - Sistemas',s=1)


plt.grid()
plt.xlabel('RA',fontsize='x-large')
plt.ylabel('Dec',fontsize='xx-large')
#plt.title('CF-{0} All Estimators'.format(sample),fontsize='x-large')
plt.legend(fancybox=True,shadow=True,frameon=True,fontsize=13,loc=2)

#plt.xlim(10.**-1., 10.**1.5) 
#plt.ylim(10**0.8, 10.**3.2)
#plt.yscale('log')
#plt.xscale('log')
plt.tight_layout()
#plt.savefig('random-main-sistemas.png')
plt.show()