# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 10:30:58 2015

@author: federico
"""
def plot(name,rp,nbins,xi_nat,xi_dp,xi_ls,xi_ham,ejn_nat,ejn_dp,ejn_ls,ejn_ham,save=False):

    sample=name
#%%
    import matplotlib.pyplot as plt
    from pylab import rcParams
    plt.close()
    rcParams['figure.figsize'] = 15, 10
    
    plt.errorbar(rp, xi_nat, yerr=ejn_nat, color='k', fmt='')
    plt.scatter(rp,xi_nat,color='k',s=25,label='Natural')
    
    plt.errorbar(rp, xi_dp, yerr=ejn_dp, color='r', fmt='')
    plt.scatter(rp,xi_dp,color='r',s=25,label='Davis-Peebles')

    plt.errorbar(rp, xi_ham, yerr=ejn_ham, color='b', fmt='')
    plt.scatter(rp,xi_ham,color='b',s=25,label='Hamilton')

    plt.errorbar(rp, xi_ls, yerr=ejn_ls, color='g', fmt='')
    plt.scatter(rp,xi_ls,color='g',s=25,label='Landy-Szalay')
    
    
    
    plt.grid()
    plt.xlabel('$\sigma$',fontsize='x-large')
    plt.ylabel('$\omega(\sigma)$',fontsize='xx-large')
    plt.title('FC-{0} Todos los Estimadores'.format(sample),fontsize='x-large')
    plt.legend(fancybox=True,shadow=True,frameon=True,fontsize=20)
    
    #plt.xlim(10.**-1., 10.**1.5) 
    #plt.ylim(10**0.8, 10.**3.2)
    plt.yscale('log')
    plt.xscale('log')
    plt.tight_layout()
    if save==True: 
        plt.savefig('graf_ws_hist2d_{0}nbins_{1}.png'.format(nbins,sample))
    plt.show()