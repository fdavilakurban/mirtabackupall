# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 12:09:25 2015


@author: federico
"""
#%% Parámetros de la Apertura

rpmin = 1. # Radio Min
rpmax = 2. # y Max del anillo 

dV = 1000. # Delta Velocidad[km/s] (Media Altura del Cilindro)

sample = 'paresc' # Muestra a analizar

#%% Otros Parámetros
Om = 0.3
c = 300000.
H_0 = 70.

zmax = dV/H_0 # (dV/c)*(c/H_0)=dz[Mpc]
zmin = 0.0001


#%% Lectura de Datos

#t1 = time.time()

galf  = '{}.txt'.format(sample) # data sample
#if sample == 'todos': galf = 'SCG_centros_maskedallgxs_Compactos_clean.dat'
#calf  = 'MGS_DR10_maskedIDgr.fits' # cross sample
calf = 'random_MGS_DR10_z.dat'

fr = fast_reader={'parallel': True, 'use_fast_converter': True}
print 'Reading file: ', galf
gals = ascii.read(galf,delimiter=' ',fast_reader=fr)
print '--> nr of rows : ', len(gals)
print 'Reading file: ', calf
#hdulist = fits.open(calf,memmap=True)
#cals = Table(hdulist[1].data)
cals = ascii.read(calf,delimiter=' ',fast_reader=fr)
print '--> nr of rows : ', len(cals)

#t2 = time.time()

#print 'Time spent reading files: ',t2-t1,'s'

for name in cals.colnames:
    if name == 'ra_1':
        cals.rename_column('ra_1','ra')
        cals.rename_column('dec_1','dec')
    
nt=len(gals)
#%% Restriccion en Mr
#print 'Restricting Tracers in Luminosity...'
#x=np.where(cals['Mr']>=-19.)
#cals.remove_rows(x)
#print 'Done'
#print 'Final nr of cross rows:', len(cals)
        
#%% Pasaje de Unidades
#t1=time.time()
gals['Xcent'] *= np.pi/180. # a radianes
gals['Ycent'] *= np.pi/180.
gals['zprom'] *= c/H_0      # a Mpc

cals['ra'] *= np.pi/180. # a radianes
cals['dec']*= np.pi/180.
cals['z']  *= c/H_0      # a Mpc

#t2=time.time()
#print t2-t1

#%% Calculo del Num de Bins para Crear la Grilla
t1=time.time()

a1=sorted(gals['Xcent'])
d1=sorted(gals['Ycent'])
z1=sorted(gals['zprom'])

rpmax1 = 10. # Aprox. 10 para optimizar 
zz = z1[0]*H_0/c # z min de la muestra
rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5)))) / (H_0*(Om**2)*(1.+zz)**2)

t2=time.time()

nbins=int((a1[-1]-a1[0])/(rpmax1/rz)),int((d1[-1]-d1[0])/(rpmax1/rz))

print 'Time spent calculating bins:',t2-t1,'s'

print 'Nbins=',nbins
#%% Renombro algunas columnas para usar en el hist2d
gals.rename_column('Xcent','ra')
gals.rename_column('Ycent','dec')
gals.rename_column('zprom','z')
gals.rename_column('name','groupID')


#%% Creación de las Matrices de Datos
print 'Creating the coordenates matrices...'
t1=time.time()
import hist2drec_fede
#nbins=3,3
print 'Creating Data Matrix...'
c_data,H_d=hist2drec_fede.hist2d_fede(gals,nbins)#,plot=True,plttitle='Data')
print 'Data Matrix created'
print 'Creating Cross Matrix...'
c_cross,H_c=hist2drec_fede.hist2d_fede(cals,nbins)#,plot=True,plttitle='Cross') 
print 'Cross Matrix created'
t2=time.time()

print 'Time spent creating the matrices: ',(t2-t1)/60.,'m'

#%% Conteo de los Vecinos
t1 = time.time()
iflag=0
jflag=0
cts=[]

for i in range(nbins[1]):
    print i,'/',nbins[1]-1
    for j in range(nbins[0]):
        #print 'i,j:',i,j
        #if c_data[i,j,0,0] == 0.: continue # Revisar: no parece afectar
        for k in range(int(H_d[i,j])):
            
            z1 = c_data[i,j,k,2]+zmax
            z2 = c_data[i,j,k,2]-zmax
            zc = c_data[i,j,k,2]*H_0/c 
            cont = 0
            if  zc == 0.: 
                print 'zc=0;',i,j,k,'id:',c_data[i,j,k,3]                
                continue 
            for i1 in range(i-1,i+2):

                if i1==nbins[1]: 
                    i1 = 0
                    iflag = 1
                
                for j1 in range(j-1,j+2):
                    #print 'i1,j1',i1,j1
                    if j1==nbins[0]: 
                        j1 = 0
                        jflag = 1
                        
                    for k1 in range(int(H_c[i1,j1])):
                        #print i1,j1,k1
                        if c_cross[i1,j1,k1,2] >= z1 or c_cross[i1,j1,k1,2] <= z2: continue
                        if c_data[i,j,k,3] == c_cross[i1,j1,k1,3]:
                            #print i,j,k,'/',i1,j1,k1,':','Pertenecientes al mismo grupo:',c_data[i,j,k,3],c_cross[i1,j1,k1,3]
                            continue
                        zv = c_cross[i1,j1,k1,2]*H_0/c              
                        if  zv == 0.: 
                            print i,j,k,k1,'zv=0'
                            continue 

#-----------------------Calculo de la Dist Proy------------------------
                        zz = (zc+zv)/2.
                        rz = (2.*c* (2.-Om+Om*zz-((2.-Om)*((1.+Om*zz)**0.5)))) / (H_0*(Om**2)*(1.+zz)**2)
                        if rz==0.: 
                            print 'rz=0'                            
                            continue
                        if rz <=0.1: print 'rz chico'    
                        da = abs(c_cross[i1,j1,k1,0]-c_data[i,j,k,0])
                    
                        if da > np.pi: da = 2.*np.pi-da
                        if da == 0.: continue
                            
                        dd = (np.sin(c_cross[i1,j1,k1,1])*np.sin(c_data[i,j,k,1])) + (np.cos(c_cross[i1,j1,k1,1])*np.cos(c_data[i,j,k,1]))*np.cos(da)
                        if dd==0.: print 'dd=0'        
                        if abs(dd) > 1.:                    
                                if dd < -1.: dd=-1.
                                if dd > 1.: dd=1.
                        dd = np.arccos(dd)    
                        rp = rz*dd
                        
                        if rp > rpmax or rp < rpmin: continue
                        cont += 1
                        
                        #print 'rp =',rp,i,j,'/',nbins[1]-1
                        #if rp<0.3:
                            #print 'z=',c_cross[i1,j1,k1,2]*H_0/c
                            

                        
                    if jflag == 1: 
                        j1=nbins[0]
                        jflag=0

                if iflag == 1: 
                    i1=nbins[1]
                    iflag=0
            
            #if cont != 0: cts.append([cont,c_data[i,j,k,3]])    
            cts.append([cont,c_data[i,j,k,3]])
t2=time.time()

print (t2-t1)/60.,'m'
#%% Cálculo de la Densidad   
rho=[]
for n,group_id in cts:
    rho.append([(n/(2*zmax*np.pi*(rpmax-rpmin)**2)),group_id]) # Densidad 

#%% Escritura de Archivos
#filename='fixap_{0}_{1}-{2}-{3}.txt'.format(sample,rpmin,r#pmax,int(dV))
#a=np.reshape(rho,(len(rho),2))
#ascii.write(a,filename,format='no_header')
#print 'File "{}" created'.format(filename)

        
