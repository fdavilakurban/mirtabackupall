# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 09:44:16 2015

INCOMPLETO


@author: vvvteam
"""

from astropy.io import ascii
import matplotlib.pyplot as plt
from pylab import rcParams
rcParams['figure.figsize'] = 15, 10
plt.close()

#%%
#plt.figure(1)
#plt.subplot(311)
estimator = 'nat'

par = ascii.read('output_hist2d.pr.{}_paresc'.format(estimator),delimiter=' ')
tri = ascii.read('output_hist2d.pr.{}_tripletes'.format(estimator),delimiter=' ')
gm = ascii.read('output_hist2d.pr.{}_gmenores'.format(estimator),delimiter=' ')


#plt.errorbar(par['col1'], par['col2'], yerr=par['col3'], color='k', fmt='')
#plt.scatter(par['col1'], par['col2'], color='k', s=25, label='Pares')
#
#plt.errorbar(tri['col1'], tri['col2'], yerr=tri['col3'], color='r', fmt='')
#plt.scatter(tri['col1'], tri['col2'], color='r', s=25, label='Tripletes')
#
#plt.errorbar(gm['col1'], gm['col2'], yerr=gm['col3'], color='b', fmt='')
#plt.scatter(gm['col1'], gm['col2'], color='b', s=25, label='Grupos Menores')
#
#plt.grid()
#plt.xlabel('$\sigma$',fontsize='x-large')
#plt.ylabel('$\omega(\sigma)$',fontsize='xx-large')
#plt.title('FC ({}) - Todos los Sistemas'.format(estimator),fontsize='x-large')
#plt.yscale('log')
#plt.xscale('log')
#plt.tight_layout()

#%%

f, (ax1, ax2, ax3) = plt.subplots(4, sharex=True, sharey=True)
ax1.plot(x, y)
ax1.set_title('Sharing both axes')
ax2.scatter(x, y)
ax3.scatter(x, 2 * y ** 2 - 1, color='r')
# Fine-tune figure; make subplots close to each other and hide x ticks for
# all but bottom plot.
f.subplots_adjust(hspace=0)
plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)


plt.grid()
plt.xlabel('$\sigma$',fontsize='x-large')
plt.ylabel('$\omega(\sigma)$',fontsize='xx-large')
plt.title('FC ({}) - Todos los Sistemas'.format(estimator),fontsize='x-large')
plt.legend(fancybox=True,shadow=True,frameon=True,fontsize=20)

#plt.xlim(10.**-1., 10.**1.5) 
#plt.ylim(10**0.8, 10.**3.2)
plt.yscale('log')
plt.xscale('log')
plt.tight_layout()
#plt.savefig('graf_ws_ls.png')
plt.show()